#! /usr/bin/python

import ast
from setuptools import setup, find_packages

with open('pemcoupling/version.py') as f:
    for line in f:
        if line.startswith('__version__'):
            version = ast.parse(line).body[0].value.s
            break

setup(
    name='pemcoupling',
    version=version,
    url='http://pem.ligo.org',
    author='Philippe Nguyen',
    author_email='philippe.nguyen@ligo.org',
    description='PEM Coupling Function Calculator',
    license='GNU General Public License Version 3',
    packages=find_packages(),
    scripts=[
        'bin/pemcoupling',
        'bin/pemcoupling-composite',
        'bin/pemcoupling-sitewide'
    ]
)
