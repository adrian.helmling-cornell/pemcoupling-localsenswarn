#! /usr/bin/env python

import h5py
import numpy as np
import os
import pandas as pd
from argparse import ArgumentParser

MAX_FREQ = 1024
COMPLEVEL = 9
COMPLIB = 'zlib'

desc = """Combine txt files produced by pemcoupling-composite into a single .h5 file.

Example:
  python pemcheck-txt-to-h5.py pemcheck-coupling-functions-H1.h5 files/H1:*.txt -m "H1 coupling functions from start of O3"
"""
parser = ArgumentParser(description=desc)
parser.add_argument("output", help="Output .h5 file")
parser.add_argument("files", nargs="*", help="Coupling function .txt files")
parser.add_argument("-m", "--comment", help="add short description for the output h5 file")
args = parser.parse_args()
output = args.output
files = args.files
if args.comment:
    comment = args.comment
else:
    comment = raw_input("Add a short description to the output h5 file: ")

if os.path.exists(output):
    os.remove(output)
for file in files:
    channel = (
        os.path.basename(file)
        .replace('_composite_coupling_data.txt', '')
        .replace(':', '_')
        .replace('-', '_')
        .replace('_DQ', '') + '_DQ'
    )
    print(channel)
    cf_file = os.path.abspath(file)
    df = pd.read_csv(cf_file, comment='#')
    df = df[df.frequency < MAX_FREQ]
    if np.any(df.flag != 'No data'):
        df.to_hdf(output, channel, format='table', data_columns=df.columns,
                  complevel=COMPLEVEL, complib=COMPLIB)
    else:
        print("Coupling function for {} is empty".format(channel))

with h5py.File(output, 'a') as f:
    f.attrs.create('comment', comment)

print("file size = {:.1f}M".format(os.stat(output).st_size / (2 ** 20)))
