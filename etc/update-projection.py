#! /usr/bin/python

import numpy as np
import os
from glob import glob
import re
import scipy.io as spio
import matplotlib
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

from gwpy.timeseries import TimeSeries
from gwpy.frequencyseries import FrequencySeries
from gwpy.time import tconvert, from_gps

matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'arial'

# =============================================================================

DEFAULT_AXIS_X0 = 0.1
DEFAULT_AXIS_X1 = 0.98
DEFAULT_AXIS_Y0 = 0.17
DEFAULT_AXIS_Y1 = 0.88
DEFAULT_AXIS_POS = [
    DEFAULT_AXIS_X0,
    DEFAULT_AXIS_Y0,
    DEFAULT_AXIS_X1 - DEFAULT_AXIS_X0,
    DEFAULT_AXIS_Y1 - DEFAULT_AXIS_Y0
]
PLOT_PARAMS = {
    'xlabel_fontsize'  : 25,
    'ylabel_fontsize'  : 25,
    'ylabel_pad'       : 30,
    'title_y'          : 1.01,
    'title_fontsize'   : 30,
    'tick_labelsize'   : 25,
    'caption_fontsize' : 18,
    'caption_x'        : 0,
    'caption_y'        : -0.2,
    'lgd_fontsize'     : 18,
    'lgd_loc'          : 'upper right',
    'lgd_alpha'        : 0.5
}

# =============================================================================


def flag2int(flags):
    flag_key = {
        'Real'               : 0,
        'Upper Limit'        : 1,
        'Thresholds not met' : 2,
        'No data'            : 3
    }
    out = []
    for f in flags:
        if f in flag_key.keys():
            out.append(flag_key[f])
    return out


def quad_sum_names(channel_names):
    mag_pattern = '.*PEM-.*_MAG_.*_(X|Y|Z)'
    wfs_pattern = '.*IMC-WFS_.*(_PIT_|_YAW_)'
    qsum_dict = {}
    for name in channel_names:
        short_name = name.rstrip('_DQ')
        mag_match = re.search(mag_pattern, short_name)
        wfs_match = re.search(wfs_pattern, short_name)
        if mag_match is not None or wfs_match is not None:
            if mag_match is not None:
                qsum_name = short_name[:-2] + '_XYZ'
            elif wfs_match is not None:
                if '_PIT_' in short_name:
                    qsum_name = short_name.replace(
                        '_PIT_OUT', '_PITYAW_QUADSUM')
                else:
                    qsum_name = short_name.replace(
                        '_YAW_OUT', '_PITYAW_QUADSUM')
            if qsum_name not in qsum_dict.keys():
                qsum_dict[qsum_name] = [name]
            else:
                qsum_dict[qsum_name].append(name)
    qsum_dict = {
        name: axes for name, axes in qsum_dict.items() if len(axes) > 1}
    return qsum_dict


def get_gwinc(filename, name):
    mat_dict = get_mat(filename)
    data = mat_dict['nnn']
    gwinc = FrequencySeries(
        data['Total'], frequencies=data['Freq'], name=name)
    return gwinc


def get_mat(filename):
    """
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    """
    data = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)


def _check_keys(d):
    """
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    """
    for key in d:
        if isinstance(d[key], spio.matlab.mio5_params.mat_struct):
            d[key] = _todict(d[key])
    return d


def _todict(matobj):
    """
    A recursive function which constructs from matobjects nested dictionaries
    """
    d = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            d[strg] = _todict(elem)
        else:
            d[strg] = elem
    return d


def update_ambient(data, asd):
    freqs = asd.frequencies.value
    cf_interp = np.interp(freqs, data['frequency'], data['factor'])
    flag_interp = np.interp(freqs, data['frequency'], data['flag'])
    ambient = cf_interp * asd.value
    flags = np.rint(flag_interp).astype(int)
    return freqs, flags, ambient


def projection_plot(
    freqs, flags, ambient, darm, channel, filename, duration,
    plot_darm=False, gwinc=None,
    x_min=None, x_max=None,
    y_min=None, y_max=None
):
    freqs_measured = freqs[flags == 0]
    freqs_upper_limit = freqs[flags == 1]
    ambient_measured = ambient[flags == 0]
    ambient_upper_limit = ambient[flags == 1]
    if x_min is None:
        x_min = freqs.min()
    if x_max is None:
        x_max = freqs.max()
    y_values = np.concatenate((
        ambient[(ambient > 0) & (freqs >= x_min) & (freqs <= x_max)],
        darm.value[
            (darm.value > 0) &
            (darm.frequencies.value >= x_min) &
            (darm.frequencies.value <= x_max)]
    ))
    if y_min is None:
        y_min = y_values.min() / 4
        if not plot_darm:
            y_min /= 4000
    if y_max is None:
        y_max = y_values.max() * 10
        if not plot_darm:
            y_max /= 4000
    # Plotting
    figure, ax = plt.subplots(1, 1, figsize=(18, 12))
    ax.set_position(DEFAULT_AXIS_POS)
    if gwinc is not None:
        lgd_patches = [
            mpatches.Patch(color=color)
            for color in ['k', '0.5', 'r', 'b']]
        lgd_labels = [
            darm.name, gwinc.name,
            'Measured Value (Signal seen in both sensor and DARM)',
            'Upper Limit (Signal seen in sensor but not DARM)']
    else:
        lgd_patches = [
            mpatches.Patch(color=color)
            for color in ['k', 'r', 'b']]
        lgd_labels = [
            darm.name,
            'Measured Value (Signal seen in both sensor and DARM)',
            'Upper Limit (Signal seen in sensor but not DARM)']
    if plot_darm:
        ax.loglog(
            darm.frequencies.value, darm.value / 10, 'k--', alpha=0.6)
        ax.loglog(darm.frequencies.value, darm.value, 'k')
        if gwinc is not None:
            ax.loglog(
                gwinc.frequencies.value, gwinc.value, color='0.5', lw=3)
        ax.loglog(
            freqs_upper_limit, ambient_upper_limit, 'bo', ms=3,
            markeredgewidth=0)
        ax.loglog(
            freqs_measured, ambient_measured, 'ro', ms=3, markeredgewidth=0)
    else:
        ax.loglog(
            darm.frequencies.value, darm.value / 10 / 4000, 'k--', alpha=0.6)
        ax.loglog(
            darm.frequencies.value, darm.value / 4000, 'k')
        if gwinc is not None:
            ax.loglog(
                gwinc.frequencies.value, gwinc.value / 4000, color='0.5', lw=3)
        ax.loglog(
            freqs_upper_limit, ambient_upper_limit / 4000,
            'bo', ms=3, markeredgewidth=0)
        ax.loglog(
            freqs_measured, ambient_measured / 4000,
            'ro', ms=3, markeredgewidth=0)
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(PLOT_PARAMS['tick_labelsize'])
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(PLOT_PARAMS['tick_labelsize'])
    ax.set_xlabel('Frequency [Hz]', size=PLOT_PARAMS['xlabel_fontsize'])
    ax.set_ylabel(
        'Strain ASD [1/sqrt(Hz)]', ha='center', va='center',
        labelpad=PLOT_PARAMS['ylabel_pad'],
        size=PLOT_PARAMS['ylabel_fontsize'])
    ax.set_title(
        channel.rstrip('_DQ').replace('_', ' ') + ' - Noise Projection',
        va='bottom', ha='center', y=PLOT_PARAMS['title_y'],
        size=PLOT_PARAMS['title_fontsize'])
    ax.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    ax.minorticks_on()
    ax.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    ax.legend(
        lgd_patches, lgd_labels,
        prop={'size': PLOT_PARAMS['lgd_fontsize']},
        loc=PLOT_PARAMS['lgd_loc'],
        alpha=PLOT_PARAMS['lgd_alpha'])
    bkgd_timestamp = from_gps(tconvert(asd.epoch))\
        .strftime('%Y-%m-%d %H:%M:%S')
    plot_timestamp = from_gps(tconvert()).strftime('%Y-%m-%d %H:%M:%S')
    ax.text(
        0, -0.09, 'Sensor quiet time: ' + bkgd_timestamp, ha='left',
        size=PLOT_PARAMS['caption_fontsize'], transform=ax.transAxes)
    ax.text(
        0, -0.14, 'Plot generated: ' + plot_timestamp, ha='left',
        size=PLOT_PARAMS['caption_fontsize'], transform=ax.transAxes)
    ax.text(
        1, -0.09, 'BW: %.5f Hz' % asd.df.value, ha='right',
        size=PLOT_PARAMS['caption_fontsize'], transform=ax.transAxes)
    ax.text(
        1, -0.14, 'Duration: %.0f s' % duration, ha='right',
        size=PLOT_PARAMS['caption_fontsize'], transform=ax.transAxes)
    plt.savefig(filename)
    plt.close()
    return


def projection_txt(freqs, flags, ambient, filename):
    with open(filename, 'w') as file:
        header = [
            '# Flags key: 0 = Measured value, 1 = Upper Limit, ' +
            '2 = Thresholds not met, 3 = No data\n',
            '# Ambient values are always given in DARM units [m/sqrt(Hz)]\n',
            'frequency,flag,ambient\n'
        ]
        file.writelines(header)
        for i in range(len(freqs)):
            row = '%.2f,%s,%.2e\n' % (freqs[i], flags[i], ambient[i])
            file.write(row)
    return


def load_coupling_functions(directory, channels=[]):
    files = glob(os.path.join(directory, '*_coupling_data.txt'))
    if len(channels) == 0:
        channels = [
            os.path.basename(f).rstrip('_composite_coupling_data.txt')
            for f in files]
    else:
        channel_basenames = [c.rstrip('_DQ') for c in channels]
        files = [
            f for f in files
            if os.path.basename(f).rstrip('s_composite_coupling_data.txt')
            in channel_basenames]
    cf_dict = {}
    for c, f in zip(channels, files):
        cf_dict[c] = {}
        with open(f, 'r') as cf_file:
            lines = [x.rstrip().split(',') for x in cf_file.readlines()]
            names = lines[0]
            columns = list(zip(*lines[1:]))
            for n, col in zip(names, columns):
                if n != 'flag':
                    cf_dict[c][n] = np.array([float(x) for x in col])
                else:
                    cf_dict[c][n] = np.array(flag2int(col), dtype=np.int64)
    return cf_dict

# =============================================================================


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument(
        "start", help="Start of quiet sensor time for new projection.")
    parser.add_argument(
        "end", help="End of quiet sensor time for new projection.")
    parser.add_argument(
        "-c", "--channel", default=[], action="append", dest="channels",
        help="Single channel name. You can " +
        "include a custom calibration value by joining with " +
        "a comma, e.g. L1:IMC-WFS_A_I_YAW_OUT_DQ,-6.8e-3")
    parser.add_argument(
        "-C", "--channel-list",
        help="File containing channels to look for.")
    parser.add_argument(
        "-d", "--directory", default=None,
        help="Directory containing coupling functions.")
    parser.add_argument(
        "-o", "--output", default=None,
        help="Directory where new projections will be saved.")
    parser.add_argument(
        "-a", "--fft-averages", type=int, default=51,
        help="Number of FFT averages.")
    parser.add_argument(
        "-g", "--gwinc-file", default=None,
        help="A .mat file containing GWINC noise curve.")
    parser.add_argument(
        "--gwinc-label", default="GWINC",
        help="Label for the GWINC noise curve.")
    parser.add_argument(
        "--plot-darm", action="store_true", default=False,
        help="Plot DARM instead of strain.")
    parser.add_argument(
        "--x-min", type=float, default=None,
        help="Minimum plot frequency.")
    parser.add_argument(
        "--x-max", type=float, default=None,
        help="Maximum plot frequency.")
    parser.add_argument(
        "--y-min", type=float, default=None,
        help="Minimum plot ASD value (.")
    parser.add_argument(
        "--y-max", type=float, default=None,
        help="Maximum plot frequency.")
    args = parser.parse_args()
    if args.output is not None:
        output = args.output
    else:
        output = os.getcwd()
    if args.directory is not None:
        directory = args.directory
    else:
        directory = os.getcwd()
    if '-' not in args.start:
        start = tconvert(args.start)
    else:
        start = tconvert() + float(args.start)
    if '-' not in args.end:
        end = tconvert(args.end)
    else:
        end = tconvert() + float(args.end)
    duration = float(end - start)
    overlap = 0.5
    fft_length = duration / (1 + overlap * (args.fft_averages - 1))
    x_min = args.x_min
    x_max = args.x_max
    y_min = args.y_min
    y_max = args.y_max
    calib_dict = {}
    for i, c in enumerate(args.channels):
        if ',' in c:
            x = c.split(',')
            calib_dict[x[0]] = float(x[1])
    channels = sorted(calib_dict.keys())
    if args.gwinc_file is not None:
        gwinc = get_gwinc(args.gwinc_file, name=args.gwinc_label)
    else:
        gwinc = None
    qsum_dict = quad_sum_names(channels)
    channels += qsum_dict.keys()
    cf_dict = load_coupling_functions(directory, channels)
    asd_dict = {}
    for c, d in cf_dict.items():
        if 'QUADSUM' in c:
            continue
        ts = TimeSeries.fetch(c, start, end, allow_tape=0)
        asd = ts.asd(fftlength=fft_length, overlap=overlap)
        if c in calib_dict.keys():
            asd *= calib_dict[c]
        asd = asd.crop(start=d['frequency'].min(), end=d['frequency'].max())
        asd_dict[c] = asd
        freqs, flags, ambient = update_ambient(d, asd)
        darm = FrequencySeries(
            d['darm'], frequencies=d['frequency'], name='DARM background')
        plot_filename = os.path.join(
            output, c.rstrip('_DQ') + '_projection.png')
        txt_filename = os.path.join(
            output, c.rstrip('_DQ') + '_projection.txt')
        projection_plot(
            freqs, flags, ambient, darm, c, plot_filename, duration,
            gwinc=gwinc, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)
        projection_txt(
            freqs, flags, ambient, txt_filename)
    for qsum_channel, axis_names in qsum_dict.items():
        qsum_asd = np.sqrt(sum([asd_dict[n]**2 for n in axis_names]))
        qsum_asd.name = qsum_channel
        freqs, flags, ambient = update_ambient(cf_dict[qsum_channel], qsum_asd)
        darm = FrequencySeries(
            d['darm'], frequencies=d['frequency'], name='DARM background')
        plot_filename = os.path.join(
            output, qsum_channel.rstrip('_DQ') + '_projection.png')
        txt_filename = os.path.join(
            output, qsum_channel.rstrip('_DQ') + '_projection.txt')
        projection_plot(
            freqs, flags, ambient, darm, qsum_channel, plot_filename, duration,
            gwinc=gwinc, x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max)
        projection_txt(
            freqs, flags, ambient, txt_filename)
