.. pemcoupling documentation master file, created by
   sphinx-quickstart on Wed Dec 12 21:18:40 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pemcoupling's documentation!
=======================================

The ``pemcoupling`` package provides tools for computing coupling functions and more.

The code can be found at the `git lab repository <https://git.ligo.org/philippe.nguyen/pemcoupling>`_.

See the `PEM website <http://pem.ligo.org>`_ for more information on the PEM subystem.

Contents:

.. toctree::
   :maxdepth: 2

   install
   command-line/index
   module/index
   how-it-works/index