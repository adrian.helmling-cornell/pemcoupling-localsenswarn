.. _usage-composite:

pemcoupling-composite
=====================

`Composite coupling functions <../how-it-works/how-composite>`_
can be produced directly after producing coupling functions via
``pemcoupling``: ::

    pemcoupling config.ini ACC -I injections.txt --composite

or from pre-existing coupling functions using the stand-alone
``pemcoupling-composite`` command: ::

    pemcoupling-composite config.ini ACC

By default ``pemcoupling-composite`` searches the existing directory for
subdirectories containing coupling functions. Results are saved to another
subdirectory, name given by the ``composite_dir`` option in ``config.ini``.

See :ref:`channels` for instructions on using a custom channel or channel
list, or searching for channel string patterns.

Full command-line help menu
---------------------------

.. program-output:: pemcoupling-composite --help
