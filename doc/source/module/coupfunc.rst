.. _coupfunc:

The CoupFunc object
====================

The ``CoupFunc`` object is used to generated a coupling function from
sensor and DARM spectra. It can be created from an ``Injection`` object
via the ``get_coupling_functions()`` method, loaded from file with
``CoupFunc.load(<filename>)``, or computed via ``CoupFunc.compute(<args>)``.