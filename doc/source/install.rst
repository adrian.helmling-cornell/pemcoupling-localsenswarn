.. _install:

Installation
============

Clone this git reposity via ::

    git clone https://git.ligo.org/philippe.nguyen/pemcoupling.git

and install locally via ::

    python setup.py install --user

Or pip install directly from git (which clones the repository first) via ::

    pip install --user git+https://git.ligo.org/philippe.nguyen/pemcoupling.git

You don't need ``--user`` if you are installing in a virtualenv.