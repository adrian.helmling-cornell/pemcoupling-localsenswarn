# PEM Coupling Function Tools

Contact: Adrian Helmling-Cornell (adrian.helmling-cornell@ligo.org).

This package contains tools for calculating coupling functions for PEM channels.

# Documentation

See https://pemcoupling.readthedocs.io/en/stable/ for full documentation.

# Installation

Python 3.6+ required.
Clone this git reposity via

```
git clone https://git.ligo.org/pem/pemcoupling.git

cd pemcoupling
```

and install in a new [conda](https://docs.conda.io/en/latest/) environment:
```
conda create -n pem_env python=3

conda activate pem_env

conda install -c conda-forge setuptools=58.2.0

conda install -c conda-forge python-nds2-client

python -m pip install .

pip install -r requirements.txt
```

If using this on LDG machines, make sure to source conda first:

```
source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
```

For development, it is convenient to install in "edit mode" instead:
```
python -m pip install -e .
```

# Some basic examples

See the full documentation for more examples. The miminal arguments required for
``pemcoupling`` are a configuration file, the section(s) of the config file to
be used, and background/injection times.

Compute coupling function for a single background and injection time:

```
pemcoupling <config file> <config section> -t 1185815495 1185821351
```

Compute coupling function for a single channel instead of all the channels in
the config file:

```
pemcoupling <config file> <config section> -c channel_name H1:PEM-CS_MAG_LVEA_INPUTOPTICS_X_DQ -t 1185815495 1185821351
```

You can add more ``-c channel_name`` arguments to append more channels to the
analysis. If all three axes of a tri-axial magnetometer are provided, a
quadrature-summed channel (ending in "XYZ") will be automatically generated as
well. To include custom calibration factors for a channel, e.g. with WFS, append
these to the channel names with a comma: `-c L1:IMC_WFS_YAW_I_OUT_DQ,-6.8e-3`

Compute coupling functions for all vibrational channels (i.e. the ACC and MIC
sections in the config file), using times from a formatted injection table txt
file:

```
pemcoupling <config file> ACC MIC -I injections.txt
```

Compute coupling functions for all vibrational channels (i.e. the ACC and MIC
sections in the config file), with times provided by DTT files in the specified
directory, and compute composite coupling functions when complete:

```
pemcoupling <<config file> ACC MIC --composite -d <DTT directory>
```

See https://pemcoupling.readthedocs.io/en/stable/ for more examples.
