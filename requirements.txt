astropy >= 1.3.3
gwpy >= 1.0.1
h5py >= 1.3
ligo-segments >= 1.0.0
ligotimegps >= 1.2.1
lscsoft-glue >= 2.0.0
matplotlib == 3.3.0
numpy >= 1.12.1
pandas >= 0.17.1
python-dateutil
scipy >= 0.18.1
six >= 1.12.0
sphinx
sphinxcontrib-programoutput
