from gwpy.time import from_gps
import numpy as np
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
import datetime
import logging

# Fix usetex parameter changed by gwpy
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'


def plot_cf(cf, filename, in_counts=False, timestamp=None,
         upper_lim=True, freq_min=None, freq_max=None,
         factor_min=None, factor_max=None, fig_w=18, fig_h=9,
         markers=('o', 'o'), markersizes=(3, 3), edgewidths=(0, 0),
         facecolors=('dodgerblue', 'grey'), edgecolors=('none', 'none')):
    """
    Export a coupling function plot from the data

    Parameters
    ----------
    filename : str
    in_counts : bool
        If True, convert coupling function to counts, and treat data
        as such.
    timestamp : time.time object
        Timestamp object.
    upper_lim : bool
        If True, include upper limits in the plot. Otherwise just plot
        measured values.
    freq_min : float
        Minimum frequency (x-axis).
    freq_max : float
        Maximum frequency (x_axis).
    factor_min : float
        Minimum value of coupling factor axis (y-axis).
    factor_max : float
        Maximum value of coupling factor axis (y-axis).
    fig_w : float or int
        Figure width.
    fig_h : float or int
        Figure height.
    """

    if in_counts:
        factors = cf.values_in_counts
        unit = 'Counts'
        type_c = ''
    else:
        factors = cf.values
        unit = str(cf.unit)
        if unit == 'None' or unit == 'ct':
            unit = 'Counts'
        type_c = cf.coupling
    # Create time stamp if not provided
    if timestamp is None:
        timestamp = time.time()
    # X-AXIS LIMIT
    if freq_min is None:
        freq_min = cf.freqs[0]
    if freq_max is None:
        freq_max = cf.freqs[-1]
    # Y-AXIS LIMITS
    factor_values = factors[
        np.isfinite(factors) & (factors > 1e-30) &
        (cf.flags != 'Thresholds not met')]
    if len(factor_values) == 0:
        logging.info(
            "No coupling factors found for channel {}".format(cf.name))
        return
    if factor_min is None:
        factor_min = min(factor_values) / 3
    if factor_max is None:
        factor_max = max(factor_values) * 3
    # ORGANIZE DATA FOR COUPLING FUNCTION PLOT
    measured = [[], []]
    upper = [[], []]
    for i in range(len(cf.freqs)):
        if (cf.flags[i] == 'Upper Limit'):
            upper[1].append(factors[i])
            upper[0].append(cf.freqs[i])
        elif (cf.flags[i] == 'Measured'):
            measured[1].append(factors[i])
            measured[0].append(cf.freqs[i])
    # PLOT PROPERTIES
    marker_measured, marker_upper = markers
    ms_measured, ms_upper = markersizes
    ew_measured, ew_upper = edgewidths
    fc_measured, fc_upper = facecolors
    ec_measured, ec_upper = edgecolors

    # CREATE FIGURE FOR COUPLING FUNCTION PLOT
    fig = plt.figure()
    ax = fig.add_subplot(111)
    fig.set_figheight(fig_h)
    fig.set_figwidth(fig_w)
    ax_x0 = 0.09
    ax_x1 = 0.99
    ax_y0 = 0.17
    ax_y1 = 0.88
    ax_pos = [ax_x0, ax_y0, ax_x1 - ax_x0, ax_y1 - ax_y0]
    ax.set_position(ax_pos)
    # PLOT COUPLING FUNCTION DATA
    plt.plot(
        measured[0], measured[1], marker_measured,
        markersize=ms_measured,
        markerfacecolor=fc_measured,
        markeredgecolor=ec_measured,
        markeredgewidth=ew_measured,
        label='Measured Value',
        zorder=6)
    if upper_lim:
        plt.plot(
            upper[0], upper[1], marker_upper,
            markersize=ms_upper,
            markerfacecolor=fc_upper,
            markeredgecolor=ec_upper,
            markeredgewidth=ew_upper,
            label='Upper Limits',
            zorder=2)
    # CREATE LEGEND, LABELS, AND TITLES
    legend = plt.legend(prop={'size': 18}, loc=1)
    legend.get_frame().set_alpha(0.5)
    if len(type_c) + len(unit) > 20:
        ylabel = "{} Coupling\n[m/{}]".format(type_c, unit)
        ylabel_size = 20
    else:
        ylabel = "{} Coupling [m/{}]".format(type_c, unit)
        ylabel_size = 27
    plt.ylabel(
        ylabel, ha='center', va='center', labelpad=30, size=ylabel_size)
    plt.xlabel(r'Frequency [Hz]', size=27)
    # Titles
    plt.title('{} - Coupling Function'
              .format(cf.name.replace('_', ' ')), size=30)
    plt.suptitle(
        datetime.datetime.fromtimestamp(timestamp)
        .strftime('%Y-%m-%d %H:%M:%S'), fontsize=18, y=0.99, va='top')
    # Captions
    str_inj_time = (
        'Injection Time: {}\n({})'
        .format(from_gps(cf.t_inj), cf.t_inj))
    str_quiet_time = (
        'Background Time: {}\n({})'
        .format(from_gps(cf.t_bg), cf.t_bg))
    plt.figtext(
        ax_x0, 0.01, str_inj_time, ha='left', va='bottom', fontsize=18,
        color='b')
    plt.figtext(
        ax_x1, 0.01, str_quiet_time, ha='right', va='bottom', fontsize=18,
        color='b')
    plt.figtext(
        0.5 * (ax_x1 + ax_x0), 0.01,
        'Averages: {}\nBin width: {:1.3f} Hz'.format(cf.n_avg, cf.df), ha='center', va='bottom',
        fontsize=18, color='b')
    plt.figtext(
        ax_x0, 0.99, 'Injection name:\n{} '.format(cf.injection_name),
        ha='left', va='top', fontsize=15, color='b')
    plt.figtext(
        ax_x1, 0.99, 'Measured coupling factors: {}'.format(len(measured[1])),
        ha='right', va='top', fontsize=15, color='b')
    if upper_lim:
        plt.figtext(
            ax_x1, .965,
            'Upper limit coupling factors: {}'.format(len(upper[1])),
            ha='right', va='top', fontsize=15, color='b')
    # CUSTOMIZE AXES
    plt.xlim([freq_min, freq_max])
    plt.ylim([factor_min, factor_max])
    ax.set_xscale('log', nonpositive='clip')
    ax.set_yscale('log', nonpositive='clip')
    ax.autoscale(False)
    plt.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    # EXPORT PLOT
    plt.savefig(filename)
    plt.close()
    return fig


def plot_injection(
        cf, filename, timestamp=None, est_amb=True,
        show_darm_threshold=True, upper_lim=True, freq_min=None,
        freq_max=None, spec_min=None, spec_max=None, fig_w=18,
        fig_h=9, markers=('o', 'o'), markersizes=(3, 3),
        edgewidths=(0, 0), facecolors=('dodgerblue', 'grey'),
        edgecolors=('none', 'none')):
    """
    Export an estimated ambient plot from the data.

    Parameters
    ----------
    filename : str
    timestamp : time.time object
        Timestamp object.
    est_amb : bool
        If True, show estimated ambient super-imposed on DARM spectrum.
    show_darm_threshold: bool
        If True, draw a dashed spectrum representing one order of
        magnitude below DARM.
    upper_lim : bool
        If True, include upper limits in the plot. Otherwise just
        plot measured values.
    freq_min : float
        Minimum frequency (x-axis).
    freq_max : float
        Maximum frequency (x_axis).
    spec_min : float
        Minimum value of coupling factor axis (y-axis).
    spec_max : float
        Maximum value of coupling factor axis (y-axis).
    fig_w : float or int
        Figure width.
    fig_h : float or int
        Figure height.
    """

    if timestamp is None:
        timestamp = time.time()
    try:
        freq_min = float(freq_min)
    except TypeError:
        freq_min = cf.freqs[0]
    try:
        freq_max = float(freq_max)
    except TypeError:
        freq_max = cf.freqs[-1]
    # Y-AXIS LIMITS FOR SENSOR SPECTROGRAM
    freq_mask = (cf.freqs >= freq_min) | (cf.freqs <= freq_max)
    flag_mask = (cf.flags == 'Measured') | (cf.flags == 'Upper Limit')
    if flag_mask.sum() > 0:
        freq_mask = (freq_mask & flag_mask)
    else:
        freq_mask = freq_mask
    spec_sens_min = np.min(cf.sens_bkg[freq_mask] / 2)
    spec_sens_max = np.max(cf.sens_inj[freq_mask] * 2)
    # Y-AXIS LIMITS FOR DARM/EST AMB SPECTROGRAM
    amb_values = np.concatenate(
        (cf.ambients[freq_mask], cf.darm_inj[freq_mask]),
        axis=0)
    if np.any(amb_values):
        amb_min = np.min(amb_values[amb_values > 0])
        amb_max = np.max(amb_values[~np.isnan(amb_values)])
    else:
        amb_min = cf.darm_bkg.min() / 10.
        amb_max = cf.darm_bkg.max()
    try:
        spec_min = float(spec_min)
    except TypeError:
        if show_darm_threshold:
            spec_min = min([amb_min, min(cf.darm_bkg) / 10]) / 2
        else:
            spec_min = min([amb_min, min(cf.darm_bkg)]) / 2
    try:
        spec_max = float(spec_max)
    except TypeError:
        spec_max = max([amb_max, max(cf.darm_inj)]) * 2
    # CREATE FIGURE FOR SPECTRUM PLOTS
    try:
        fig_w = float(fig_w)
    except TypeError:
        fig_w = 18
    try:
        fig_h = float(fig_h)
    except TypeError:
        fig_h = 9
    fig = plt.figure(figsize=(fig_w, fig_h))
    # PLOT SENSOR SPECTRA
    ax1 = fig.add_subplot(211)
    ax1_x0 = 0.07
    ax1_x1 = 0.99
    ax1_y0 = 0.56
    ax1_y1 = 0.91
    ax1_pos = [ax1_x0, ax1_y0, ax1_x1 - ax1_x0, ax1_y1 - ax1_y0]
    ax1.set_position(ax1_pos)
    plt.plot(
        cf.freqs, cf.sens_inj, color='r', label='Injection', zorder=5)
    plt.plot(
        cf.freqs, cf.sens_bkg, color='k', label='Background', zorder=5)
    # CREATE LEGEND AND LABELS
    plt.legend()
    unit = str(cf.unit)
    if unit == 'None' or unit == 'ct':
        unit = 'Counts'
    if len(cf.qty) + len(unit) > 20:
        ylabel = "{}\n[{}/sqrt(Hz)]".format(cf.qty, unit)
        ylabel_size = 16
    else:
        ylabel = "{} [{}/sqrt(Hz)]".format(cf.qty, unit)
        ylabel_size = 18
    plt.ylabel(ylabel, ha='center', va='center', labelpad=20, size=ylabel_size)
    plt.title(cf.name.replace('_', ' ') + ' - Spectrum', size=20)
    # CUSTOMIZE AXES
    plt.xlim([freq_min, freq_max])
    plt.ylim([spec_sens_min, spec_sens_max])
    if ((freq_max / freq_min) > 5):
        ax1.set_xscale('log', nonpositive='clip')
    ax1.set_yscale('log', nonpositive='clip')
    ax1.autoscale(False)
    plt.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    for tick in ax1.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax1.yaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    # PLOT DARM LINES
    ax2 = fig.add_subplot(212)
    ax2_x0 = 0.07
    ax2_x1 = 0.99
    ax2_y0 = 0.13
    ax2_y1 = 0.48
    ax2_pos = [ax2_x0, ax2_y0, ax2_x1 - ax2_x0, ax2_y1 - ax2_y0]
    ax2.set_position(ax2_pos)
    plt.plot(
        cf.freqs, cf.darm_inj, '-', color='r',
        label='DARM during injection', zorder=3)
    plt.plot(
        cf.freqs, cf.darm_bkg, '-', color='0.1',
        label='DARM background', zorder=4)
    if show_darm_threshold:
        plt.plot(
            cf.freqs, cf.darm_bkg / 10., '--', color='k',
            label='DARM background / 10', zorder=2)
    # PLOT ESTIMATED AMBIENT
    # plot properties
    marker_measured, marker_upper = markers
    ms_measured, ms_upper = markersizes
    ew_measured, ew_upper = edgewidths
    fc_measured, fc_upper = facecolors
    ec_measured, ec_upper = edgecolors
    if est_amb:
        measured_amb = [[], []]
        upper_amb = [[], []]
        for y in range(len(cf.freqs)):
            if cf.flags[y] == 'Upper Limit':
                upper_amb[1].append(cf.ambients[y])
                upper_amb[0].append(cf.freqs[y])
            elif cf.flags[y] == 'Measured':
                measured_amb[1].append(cf.ambients[y])
                measured_amb[0].append(cf.freqs[y])
        plt.plot(
            measured_amb[0], measured_amb[1], marker_measured,
            markersize=ms_measured,
            markerfacecolor=fc_measured,
            markeredgecolor=ec_measured,
            markeredgewidth=ew_measured,
            label='Est. Amb.',
            zorder=6)
        if upper_lim:
            plt.plot(
                upper_amb[0], upper_amb[1], marker_upper,
                markersize=ms_upper,
                markerfacecolor=fc_upper,
                markeredgecolor=ec_upper,
                markeredgewidth=ew_upper,
                label='Upper Limit Est. Amb.',
                zorder=5)
    # CREATE LEGEND AND LABELS
    legend = plt.legend(prop={'size': 12}, loc=1)
    legend.get_frame().set_alpha(0.5)
    plt.ylabel('DARM ASD [m/sqrt(Hz)]', size=18)
    plt.xlabel('Frequency [Hz]', size=18)
    if est_amb:
        plt.title(
            '{} - Estimated Ambient'
            .format(cf.name.rstrip('_DQ').replace('_', ' ')), size=20)
    else:
        plt.title('DARM - Spectrum', size=20)
    # FIGURE TEXT
    # Supertitle (timestamp)
    plt.suptitle(
        datetime.datetime.fromtimestamp(timestamp)
        .strftime('%Y-%m-%d %H:%M:%S'), ha='center', va='top', fontsize=16,
        y=0.99)
    # Miscellaneous captions
    str_inj_time = (
        'Injection Time: {}\n({})'
        .format(from_gps(cf.t_inj), cf.t_inj))
    str_quiet_time = (
        'Background Time: {}\n({})'
        .format(from_gps(cf.t_bg), cf.t_bg))
    plt.figtext(
        ax2_x0, 0.01, str_inj_time, ha='left', va='bottom', fontsize=12,
        color='b')
    plt.figtext(
        ax2_x1, 0.01, str_quiet_time, ha='right', va='bottom', fontsize=12,
        color='b')
    plt.figtext(
        0.5 * (ax2_x1 + ax2_x0), 0.01,
        'Averages: {}\nBin width: {:1.3f} Hz'.format(cf.n_avg, cf.df), ha='center', va='bottom',
        fontsize=12, color='b')
    plt.figtext(
        ax2_x0, .99, 'Injection name:\n{} '.format(cf.injection_name),
        ha='left', va='top', fontsize=14, color='b')
    if est_amb:
        plt.figtext(
            ax2_x1, .99,
            'Measured coupling factors: {}'.format(len(measured_amb[1])),
            ha='right', va='top', fontsize=14, color='b')
        if upper_lim:
            plt.figtext(
                ax2_x1, .965,
                'Upper limit coupling factors: {}'
                .format(len(upper_amb[1])),
                ha='right', va='top', fontsize=14, color='b')
    # CUSTOMIZE AXES
    plt.xlim([freq_min, freq_max])
    plt.ylim([spec_min, spec_max])
    if ((freq_max / freq_min) > 5):
        ax2.set_xscale('log', nonpositive='clip')
    ax2.set_yscale('log', nonpositive='clip')
    ax2.autoscale(False)
    plt.grid(
        b=True, which='major', color='0.0', linestyle=':', linewidth=1,
        zorder=0)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='0.6', linestyle=':', zorder=0)
    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax2.yaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    # EXPORT PLOT
    plt.savefig(filename)
    plt.close()
    return fig
