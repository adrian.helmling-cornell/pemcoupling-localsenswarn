"""
Composite coupling function calculator.
"""

import time
import os
import glob
import re
import logging
import numpy as np
import traceback
import shutil

# pemcoupling modules
from .composite_utils import CoupFuncComposite
from ..parse import parse_config, parse_args
from ..coupfunc import CoupFunc
from .. import utils as pem_utils
from .. import html
from .sitewide import main as sitewide_main


def main(args):
    t1 = time.time()
    verbose = args.verbose
    upper_lim = (not args.no_upperlim)

    # Source and target directories
    if args.directory is not None:
        directory = args.directory
    else:
        directory = os.getcwd()

    # Find coupling functions
    glob_files = glob.glob(
        os.path.join(directory, '*/*_coupling_data.txt'))
    glob_files = [f for f in glob_files if '_composite_' not in f]
    if len(glob_files) == 0:
        logging.error("No coupling data found in directory " + directory)
    cf_files = {}
    for filename in glob_files:
        basename = os.path.basename(filename)
        channel = basename.rstrip('_composite_coupling_data.txt')
        if re.search('.*:.*-.*', channel):
            if channel not in cf_files.keys():
                cf_files[channel] = [filename]
            else:
                cf_files[channel].append(filename)
        logging.info('Including data from ' + filename)

    # Config parsing - requires injection type
    config_dict = parse_config(args.config)
    try:
        sect_cfg = config_dict[args.section[0]]
    except:
        raise ValueError("invalid config section " + args.section)
    comp_cfg = config_dict['composite coupling function']
    peak_search_window = sect_cfg.get('peak_search_window', 0)
    quad_sum = sect_cfg.get('quad_sum')
    gw_signal = comp_cfg.pop('gw_signal', None)
    gwinc_file = comp_cfg.pop('gwinc_file', None)
    gwinc_label = comp_cfg.pop('gwinc_label', None)
    fig_width = comp_cfg.pop('fig_width', None)
    fig_height = comp_cfg.pop('fig_height', None)
    composite_dir = os.path.join(directory, comp_cfg.pop('composite_dir', 'CompositeCouplingFunctions'))
    summary_dir = os.path.join(directory, comp_cfg.pop('summary_dir', 'SummaryCouplingFunctions'))
    if args.output is not None:
        out_dir = args.output
    else:
        out_dir = composite_dir
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    # Arg parsing plot parameters
    if args.freq_range is not None:
        freq_min, freq_max = args.freq_range
        comp_cfg.pop('freq_min')
        comp_cfg.pop('freq_max')
    else:
        freq_min = comp_cfg.pop('freq_min', None)
        freq_max = comp_cfg.pop('freq_max', None)
    if args.cf_range is not None:
        factor_min, factor_max = args.cf_range
        comp_cfg.pop('coup_min')
        comp_cfg.pop('coup_max')
    else:
        factor_min = comp_cfg.pop('coup_min', None)
        factor_max = comp_cfg.pop('coup_max', None)
    if args.amb_range is not None:
        amb_min, amb_max = args.amb_range
        comp_cfg.pop('amb_min')
        comp_cfg.pop('amb_max')
    else:
        amb_min = comp_cfg.pop('amb_min', None)
        amb_max = comp_cfg.pop('amb_max', None)

    # Search for channels
    if len(args.channel) > 0:
        channels_from_args = args.channel
        if quad_sum:
            qsum_dict = pem_utils.quad_sum_names(args.channel)
            qsum_channels = qsum_dict.keys()
            channels_from_args += qsum_channels
        cf_files = {
            channel: filenames for channel, filenames in cf_files.items()
            if channel in channels_from_args}
    elif args.channel_list is not None:
        channels_from_cl = pem_utils.get_channels(
            args.channel_list, station=args.station)
        if quad_sum:
            qsum_dict = pem_utils.quad_sum_names(channels_from_cl)
            qsum_channels = qsum_dict.keys()
            channels_from_cl += qsum_channels
        cf_files = {
            channel: filenames for channel, filenames in cf_files.items()
            if channel in channels_from_cl}
    if args.search_channels is not None:
        cf_files_search = pem_utils.findall(
            args.search_channels, cf_files.keys())
        if len(cf_files_search) == 0:
            raise ValueError("No channels found matching search entry")
        cf_files = {k: cf_files[k] for k in cf_files_search}
    if len(cf_files) == 0:
        err_msg = "No channels to process."
        logging.error(err_msg)
        raise RuntimeError(err_msg)
    channels = sorted(cf_files.keys())
    print("\n{} channels(s) found:".format(len(channels)))
    for c in channels:
        print(c)
    print("")

    # Import GWINC data for ambient plots
    try:
        gwinc_data = np.loadtxt(gwinc_file, unpack=True)
    except (IOError, OSError):
        logging.warning("Failed to read GWINC file " + gwinc_file)
        gwinc_data = None
    if gwinc_data is None:
        logging.info("Composite estimated ambient will not show GWINC.")

    # Load coupling function files as CoupFunc objects
    cf_list_all = []
    injection_names_all = []
    for channel in channels:
        filenames = cf_files[channel]
        cf_list = []
        injection_names = []
        for filename in filenames:
            cf = CoupFunc.load(
                filename, channel=channel)
            injection_name = os.path.basename(os.path.dirname(filename))
            if pem_utils.search(args.search_injections, injection_name):
                cf_list.append(cf)
                injection_names.append(injection_name)
        if len(cf_list) == 0:
            logging.warning(
                "No coupling functions to process for " + channel)
            continue
        cf_band_width = cf_list[0].df
        cf_freq_len = len(cf_list[0].freqs)
        for cf in cf_list:
            if cf.df != cf_band_width:
                print("{} coupling functions have unequal bandwidths."
                      .format(channel))
                for i, cf in enumerate(cf_list):
                    print("{} - bandwidth: {:2e}"
                          .format(injection_names[i], cf.df))
                raise ValueError(
                    "{} coupling functions have unequal bandwidths."
                    .format(channel))
            elif len(cf.freqs) > cf_freq_len:
                print("{} coupling functions have unequal lengths."
                      .format(channel))
                cf.crop(cf_list[0].freqs[0], cf_list[0].freqs[-1])
        cf_list_all.append(cf_list)
        injection_names_all.append(injection_names)
    injection_names_unique = sorted(set(
        [k for j in injection_names_all for k in j]))
    print("{} injection(s) found:".format(len(injection_names_unique)))
    for i in injection_names_unique:
        print(i)
    print("")
    qsum_dict = pem_utils.quad_sum_names(channels)
    plot_props_merge = {}
    plot_props_split = {}
    for k, v in comp_cfg.items():
        if ('split_' in k):
            plot_props_split[k.replace('split_', '')] = v
        else:
            plot_props_merge[k] = v

    # START OF MAIN LOOP
    darm_ratios = []

    for i, cf_list in enumerate(cf_list_all):
        channel = channels[i]
        injection_names = injection_names_all[i]
        freqs_raw = np.mean([cf.freqs for cf in cf_list], axis=0)
        darm_raw = np.min([cf.darm_bkg for cf in cf_list], axis=0)
        print("Generating composite coupling function for {}."
              .format(channel))
        qsum_axes = {}
        if channel in qsum_dict.keys():
            for axis_channel in qsum_dict[channel]:
                qsum_axis = cf_list_all[channels.index(axis_channel)]
                injections_axis = injection_names_all[
                    channels.index(axis_channel)]
                qsum_axis = [
                    a for i, a in enumerate(qsum_axis) if
                    injections_axis[i] in injection_names]
                qsum_axes[axis_channel] = qsum_axis

        # COMPUTE COMPOSITE COUPLING FUNCTION
        # Convert from Hz to bins
        comp_cf = CoupFuncComposite.compute(
            cf_list,
            injection_names,
            qsum_axes=qsum_axes,
            peak_search_window=peak_search_window)
        
        # MAX DARM RATIO
        darm_ratio = (comp_cf.ambients / darm_raw)
        darm_ratio = darm_ratio[comp_cf.flags == "Measured"]
        if len(darm_ratio) > 0:
            darm_ratios.append(darm_ratio.max())
        else:
            darm_ratios.append(0)

        # X-AXIS (FREQUENCY) LIMITS
        if verbose:
            print("Determining axis limits for plots...")
        x_axis = comp_cf.freqs[comp_cf.values > 0]
        if (len(x_axis) == 0):
            print("No lowest coupling factors for {}.".format(comp_cf.name) +
                  " Data export aborted for this channel.")
            continue

        # Y-AXIS (COUPLING FACTOR) LIMITS
        if freq_max:
            values_counts_idx = (
                (comp_cf.values > 0)
                & (comp_cf.freqs >= freq_min)
                & (comp_cf.freqs < freq_max))
        else:
            values_counts_idx = (
                (comp_cf.values > 0)
                & (comp_cf.freqs >= freq_min))
        values_idx = (
            values_counts_idx &
            (comp_cf.flags != 'Thresholds not met'))
        y_axis = comp_cf.values[values_idx]
        y_axis_counts = comp_cf.values_in_counts[values_counts_idx]
        if (len(y_axis) == 0):
            print("No lowest coupling factors for " + comp_cf.name)
            print("between %s and %s Hz.".format(freq_min, freq_max))
            print("Data export aborted for this channel.")
            continue

        # SORTED NAMES OF INJECTIONS
        sorted_names = comp_cf.unique_injections
        if None in sorted_names:
            sorted_names.remove(None)

        # FILEPATHS
        base_filename = comp_cf.name + '_composite_'
        csv_filename = os.path.join(
            out_dir, base_filename + 'coupling_data.txt')
        multi_filename = os.path.join(
            out_dir, base_filename + 'coupling_multi_plot.png')
        single_filename = os.path.join(
            out_dir, base_filename + 'coupling_plot.png')
        single_counts_filename = os.path.join(
            out_dir, base_filename + 'coupling_counts_plot.png')
        if gw_signal == 'both':
            est_amb_multi_strain_filename = os.path.join(
                out_dir, base_filename + 'est_amb_strain_multi_plot.png')
            est_amb_single_strain_filename = os.path.join(
                out_dir, base_filename + 'est_amb_strain_plot.png')
            est_amb_multi_darm_filename = os.path.join(
                out_dir, base_filename + 'est_amb_darm_multi_plot.png')
            est_amb_single_darm_filename = os.path.join(
                out_dir, base_filename + 'est_amb_darm_plot.png')
        else:
            est_amb_multi_strain_filename = os.path.join(
                out_dir, base_filename + 'est_amb_multi_plot.png')
            est_amb_single_strain_filename = os.path.join(
                out_dir, base_filename + 'est_amb_plot.png')
            est_amb_multi_darm_filename = est_amb_multi_strain_filename
            est_amb_single_darm_filename = est_amb_single_strain_filename

        # CSV OUTPUT
        comp_cf.to_csv(csv_filename)
        if verbose:
            print("CSV saved.")
        if verbose:
            print("\nLowest (composite) coupling function complete for "
                  .format(channel))

        # COMPOSITE COUPLING FUNCTION PLOT
        # Split/multi-plot
        try:
            comp_cf.plot(
                multi_filename, in_counts=False, split_injections=True,
                upper_lim=upper_lim, freq_min=freq_min, freq_max=freq_max,
                factor_min=factor_min, factor_max=factor_max,
                fig_width=fig_width, fig_height=fig_height,
                **plot_props_split)
        except:
            print(traceback.format_exc())
            print("Failed to generate composite coupling multi-plot.")
        # Merged plot
        try:
            comp_cf.plot(
                single_filename, in_counts=False, split_injections=False,
                upper_lim=upper_lim, freq_min=freq_min, freq_max=freq_max,
                factor_min=factor_min, factor_max=factor_max,
                fig_width=fig_width, fig_height=fig_height,
                **plot_props_merge)
        except:
            print(traceback.format_exc())
            print("Failed to generate composite coupling plot.")
        # Merged plot in counts
        try:
            comp_cf.plot(
                single_counts_filename, in_counts=True, split_injections=False,
                upper_lim=upper_lim, freq_min=freq_min, freq_max=freq_max,
                factor_min=None, factor_max=None,
                fig_width=fig_width, fig_height=fig_height,
                **plot_props_merge)
        except:
            print(traceback.format_exc())
            print("Failed to generate composite coupling in counts plot.")
        if verbose:
            print('Composite coupling function plots complete.')

        # COMPOSITE ESTIMATED AMBIENT PLOT
        if np.any(comp_cf.flags != 'No data'):
            if gw_signal == 'strain' or gw_signal == 'both':
                # Split/multi-plot
                try:
                    comp_cf.ambientplot(
                        est_amb_multi_strain_filename,
                        gw_signal='strain', split_injections=True,
                        gwinc_data=gwinc_data, gwinc_label=gwinc_label,
                        darm_data=[freqs_raw, darm_raw],
                        freq_min=freq_min, freq_max=freq_max,
                        amb_min=amb_min, amb_max=amb_max,
                        fig_width=fig_width, fig_height=fig_height,
                        **plot_props_split)
                except:
                    print(traceback.format_exc())
                    print("Failed to generate ambient multi-plot.")
                # Merged plot
                try:
                    comp_cf.ambientplot(
                        est_amb_single_strain_filename,
                        gw_signal='strain', split_injections=False,
                        gwinc_data=gwinc_data, gwinc_label=gwinc_label,
                        darm_data=[freqs_raw, darm_raw],
                        freq_min=freq_min, freq_max=freq_max,
                        amb_min=amb_min, amb_max=amb_max,
                        fig_width=fig_width, fig_height=fig_height,
                        **plot_props_merge)
                except:
                    print(traceback.format_exc())
                    print("Failed to generate ambient plot.")
            if gw_signal == 'darm' or gw_signal == 'both':
                # Split/multi-plot
                try:
                    comp_cf.ambientplot(
                        est_amb_multi_darm_filename,
                        gw_signal='darm', split_injections=True,
                        gwinc_data=gwinc_data, gwinc_label=gwinc_label,
                        darm_data=[freqs_raw, darm_raw],
                        freq_min=freq_min, freq_max=freq_max,
                        amb_min=amb_min, amb_max=amb_max,
                        fig_width=fig_width, fig_height=fig_height,
                        **plot_props_split)
                except:
                    print(traceback.format_exc())
                    print("Failed to generate darm ambient multi-plot.")
                # Merged plot
                try:
                    comp_cf.ambientplot(
                        est_amb_single_darm_filename,
                        gw_signal='darm', split_injections=False,
                        gwinc_data=gwinc_data, gwinc_label=gwinc_label,
                        darm_data=[freqs_raw, darm_raw],
                        freq_min=freq_min, freq_max=freq_max,
                        amb_min=amb_min, amb_max=amb_max,
                        fig_width=fig_width, fig_height=fig_height,
                        **plot_props_merge)
                except:
                    print(traceback.format_exc())
                    print("Failed to generate darm ambient plot.")
            if verbose:
                print('Composite estimated ambient plots complete.')
        else:
            print('No composite coupling data for this channel.')

    # END OF MAIN LOOP

    # Create HTML report
    colors = {}
    for channel, darm_ratio in zip(channels, darm_ratios):
        log_diff = max(-1, min(0, np.log10(darm_ratio)))
        x = -int(255 * log_diff)
        # Red if ratio >= 1, white if ratio <= 0.1
        colors[channel] = '#%02x%02x%02x' % (255, x, x)
        #colors[channel] = "rgb(255, {0}, {0})".format(x)
    src = args.config
    dest = os.path.join(out_dir, 'config.ini')
    shutil.copyfile(src, dest)
    html.write_composite_report(
        out_dir,
        channels,
        config=os.path.basename(dest),
        colors=colors)

    t2 = time.time() - t1
    print("Lowest (composite) coupling functions processed. "
          "(Runtime: {:.3f} s.)\n".format(t2))
    if args.summary:
        custom_options = [
            "--directory", out_dir,
            "--output", summary_dir,
            "--search-channels", "|".join(channels)
        ]
        if freq_min is not None and freq_max is not None:
            custom_options += [
                "--freq-range", str(freq_min), str(freq_max)]
        if factor_min is not None and factor_max is not None:
            custom_options += [
                "--cf-range", str(factor_min), str(factor_max)]
        if amb_min is not None and amb_max is not None:
            custom_options += [
                "--amb-range", str(amb_min), str(amb_max)]
        custom_args = [args.config] + args.section + custom_options
        args = parse_args('pemcoupling-sitewide', custom_args)
        sitewide_main(args)
    return
