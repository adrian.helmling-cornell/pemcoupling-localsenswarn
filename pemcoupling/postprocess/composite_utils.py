"""
Tools for computing composite coupling functions.
"""

import numpy as np
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
import logging
try:
    from ..pemchannel import PEMFrequencySeries
    from .. import utils as pem_utils
except ImportError:
    logging.error('Failed to load PEM coupling modules. ' +
                  'Make sure you have all of these in the right place!')
    raise


# =======================================================================

UNITS_DICT = {
    'ACC': 'm',
    'HPI': 'm',
    'ISI': 'm',
    'MAG': 'T',
    'MIC': 'Pa',
    'SEI': 'm'
}

# Default figure and axis dimensions
DEFAULT_FIG_WIDTH = 18
DEFAULT_FIG_HEIGHT = 12
DEFAULT_AXIS_X0 = 0.1
DEFAULT_AXIS_X1 = 0.98
DEFAULT_AXIS_Y0 = 0.17
DEFAULT_AXIS_Y1 = 0.88
DEFAULT_AXIS_POS = [
    DEFAULT_AXIS_X0,
    DEFAULT_AXIS_Y0,
    DEFAULT_AXIS_X1 - DEFAULT_AXIS_X0,
    DEFAULT_AXIS_Y1 - DEFAULT_AXIS_Y0
]

# Defaults for plot labels, legends, etc.
PLOT_PARAMS = {
    'xlabel_fontsize'  : 25,
    'ylabel_fontsize'  : 25,
    'ylabel_pad'       : 30,
    'title_y'          : 1.01,
    'title_fontsize'   : 30,
    'tick_labelsize'   : 25,
    'caption_fontsize' : 18,
    'caption_x'        : 0,
    'caption_y'        : -0.2,
    'lgd_fontsize'     : 18,
    'lgd_bbox'         : (1, 1),
    'lgd_loc'          : 'upper right',
    'lgd_alpha'        : 0.5
}

# Colormap for labeling injections in composite plots
MULTIPLOT_COLORMAP = 'gist_rainbow'

# ======================================================================


def get_colors(labels, colormap=MULTIPLOT_COLORMAP):
    """
    Gives a color to each label chosen from colormap. Use default
    matplotlib color cycle if there are only 5 or fewer colors.
    """

    if len(labels) > 5:
        cmap = plt.get_cmap(colormap)
        colors = cmap(np.linspace(0, 0.95, len(labels)))
    else:
        color_cycle = ['b', 'r', 'g', 'm', 'c']
        colors = [mcolors.ColorConverter().to_rgba(c)
                  for c in color_cycle]
    colors_dict = {x: colors[i] for i, x in enumerate(labels)}
    return colors_dict


class CompositePlot(object):
    """
    Plotting class to simplify composite plotting functions.

    Attributes
    ----------
    fig : matplotlib.Figure
    ax : matplotlib.axes.Axes
    """

    def __init__(
        self, fig_width=DEFAULT_FIG_WIDTH, fig_height=DEFAULT_FIG_HEIGHT,
        ax_pos=DEFAULT_AXIS_POS, x_min=None, x_max=None, y_min=None,
        y_max=None
    ):
        if fig_width is None:
            fig_width = DEFAULT_FIG_WIDTH
        if fig_height is None:
            fig_height = DEFAULT_FIG_HEIGHT
        fig, ax = plt.subplots(1, 1, figsize=(fig_width, fig_height))
        self.fig = fig
        self.ax = ax
        self.ax.set_position(ax_pos)
        # Axes scaling
        if x_min is not None and x_max is not None:
            self.ax.set_xlim([x_min, x_max])
        if y_min is not None and y_max is not None:
            self.ax.set_ylim([y_min, y_max])
        if x_max / x_min > 100:
            self.ax.set_xscale('log', nonpositive='clip')
        if y_max / y_min > 100:
            self.ax.set_yscale('log', nonpositive='clip')
        self.ax.autoscale(False)
        # Grid lines and ticks
        self.ax.grid(
            b=True, which='major', color='0.0',
            linestyle=':', linewidth=1, zorder=0)
        self.ax.minorticks_on()
        self.ax.grid(
            b=True, which='minor', color='0.6',
            linestyle=':', zorder=0)
        # Tick sizes
        for tick in self.ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(PLOT_PARAMS['tick_labelsize'])
        for tick in self.ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(PLOT_PARAMS['tick_labelsize'])
        # Frequency axis label
        self.ax.set_xlabel(
            'Frequency [Hz]', size=PLOT_PARAMS['xlabel_fontsize'])

    def set_ylabel(
        self, ylabel,
        labelpad=PLOT_PARAMS['ylabel_pad'],
        size=PLOT_PARAMS['ylabel_fontsize']
    ):
        if '\n' in ylabel:
            size *= 0.85
        self.ax.set_ylabel(
            ylabel, ha='center', va='center',
            labelpad=labelpad, size=size)

    def set_title(
        self, title, y=PLOT_PARAMS['title_y'],
        size=PLOT_PARAMS['title_fontsize']
    ):
        if 'Quadrature sum' in title:
            size *= 0.85
        self.ax.set_title(
            title, va='bottom', ha='center', y=y, size=size)

    def caption(
        self, text, x=PLOT_PARAMS['caption_x'],
        y=PLOT_PARAMS['caption_y'],
        size=PLOT_PARAMS['caption_fontsize']
    ):
        self.ax.text(
            x, y, text, size=size, transform=self.ax.transAxes)

    def bandwidth(self, bw, fontsize=PLOT_PARAMS['caption_fontsize']):
        self.ax.text(
            1, -0.1, 'Bin width: %1.3f Hz' % bw,
            ha='right', va='bottom', size=fontsize,
            transform=self.ax.transAxes)

    def legend(
        self, patches=[], labels=[],
        size=PLOT_PARAMS['lgd_fontsize'],
        bbox_to_anchor=PLOT_PARAMS['lgd_bbox'],
        loc=PLOT_PARAMS['lgd_loc'],
        alpha=PLOT_PARAMS['lgd_alpha']
    ):
        ncol = 1
        lgd = self.ax.legend(
            patches,
            labels,
            prop={'size': size},
            bbox_to_anchor=bbox_to_anchor,
            loc=loc,
            borderaxespad=0.,
            ncol=ncol)
        lgd_w, lgd_h = self.get_lgd_dim(lgd)
        ax_w = self.ax.get_window_extent().width
        ax_h = self.ax.get_window_extent().height
        while (lgd_w > ax_w or lgd_h > 0.2 * ax_h):
            if lgd_h > 0.2 * ax_h:
                ncol += 1
            else:
                size *= 0.95
                ncol = max(1, ncol - 1)
            lgd = self.ax.legend(
                patches,
                labels,
                prop={'size': size},
                bbox_to_anchor=bbox_to_anchor,
                loc=loc,
                borderaxespad=0.,
                ncol=ncol)
            lgd_w, lgd_h = self.get_lgd_dim(lgd)
        lgd.get_frame().set_alpha(alpha)

    def get_lgd_dim(self, lgd):
        self.fig.canvas.draw()
        lgd_extent = lgd.get_window_extent()
        return lgd_extent.width, lgd_extent.height

    def plot(self, x, y, fmt='', **kwargs):
        return self.ax.plot(x, y, fmt, **kwargs)

    def save(self, filename):
        self.fig.savefig(filename, dpi=self.fig.get_dpi() * 2)
    
    def close(self):
        plt.close()


class CoupFuncComposite(PEMFrequencySeries):
    """
    Composite coupling function data for a single sensor

    Attributes
    ----------
    name : str
        Name of PEM sensor.
    freqs : array
        Frequency array.
    values : array
        Coupling factors in physical units.
    values_in_counts : array
        Coupling factors in raw counts.
    flags : array
        Coupling factor flags ('Measured', 'Upper limit', 'Thresholds not met',
        'No data').
    injections : array
        Names of injections corresponding to each coupling factor.
    sens_bg : array
        ASD of sensor background.
    darm_bg : array
        ASD of DARM background.
    ambients : array
        Estimated ambient ASD for this sensor.
    df : float
        Frequency bandwidth in Hz
    unit : str
        Sensor unit of measurement
    qty : str
        Quantity measured by sensor
    type : str
        Coupling type associated with sensor.

    Methods
    -------
    get_channel_info
        Use channel name to get unit of measurement, name of quantity measured
        by sensor, and coupling type.
    plot
        Plot coupling function from the data.
    ambientplot
        Plot DARM spectrum with estimated ambient of the sensor.
    to_csv
        Save data to csv file.
    """

    def __init__(
            self, name, freqs, values, values_in_counts, flags, injections,
            sens_bg=None, darm_bg=None, ambients=None, comb_freqs=[]):
        """
        Parameters
        ----------
        name : str
            Name of PEM sensor.
        freqs : array
            Frequency array.
        values : array
            Coupling factors in physical units.
        values_in_counts : array
            Coupling factors in raw counts.
        flags : array
            Coupling factor flags ('Measured', 'Upper limit', 'Thresholds not met',
            'No data').
        injections : array
            Names of injections corresponding to each coupling factor.
        sens_bg : array
            ASD of sensor background.
        darm_bg : array
            ASD of DARM background.
        """

        # Coupling function data
        super(CoupFuncComposite, self).__init__(name, freqs, values)
        self.values_in_counts = np.asarray(values_in_counts)
        self.flags = np.asarray(flags)
        self.injections = np.asarray(injections)
        self.comb_freqs = comb_freqs
        # ASDs
        self.sens_bkg = sens_bg
        self.darm_bkg = darm_bg
        if ambients is None and self.sens_bkg is not None:
            self.ambients = self.values * self.sens_bkg
        else:
            self.ambients = ambients
        if self.sensor in UNITS_DICT.keys():
            self.unit = UNITS_DICT[self.sensor]
        elif self.subsystem in UNITS_DICT.keys():
            self.unit = UNITS_DICT[self.subsystem]

    @property
    def unique_injections(self):
        return sorted(set([inj for inj in self.injections if inj is not None]))

    @classmethod
    def compute(
            cls, cf_list, injection_names, qsum_axes={}, peak_search_window=0):
        """
        Selects for each frequency bin the "nearest" coupling factor, i.e. the
        lowest across multiple injection locations.

        Parameters
        ----------
        cf_list : list
            CoupFunc objects to be processed.
        injection_names : list
            Names of injections.
        peak_search_window : int, optional
            Local max window in number of frequency bins.

        Returns
        -------
        comp_cf : CompositeCoupFunc object
            Contains composite coupling function tagged with flags and
            injection names
        """

        if type(cf_list) != list or type(injection_names) != list:
            err_msg = "Parameters 'cf_list' and 'injection_names' must "\
            "be lists."
            logging.error(err_msg)
            raise TypeError(err_msg)
        if len(cf_list) != len(injection_names):
            err_msg = "Parameters 'cf_list' and 'injection_names' must "\
            "be equal length."
            logging.error(err_msg)
            raise ValueError(err_msg)
        darm_bg = np.min([cf.darm_bkg for cf in cf_list], axis=0)
        freqs = cf_list[0].freqs
        nrows = freqs.size
        ncols = len(cf_list)
        rows = range(nrows)
        channel_name = cf_list[0].name
        factor_cols = [cf.values for cf in cf_list]
        factor_counts_cols = [cf.values_in_counts for cf in cf_list]
        flag_cols = [cf.flags for cf in cf_list]
        injection_cols = [[n] * nrows for n in injection_names]
        sens_bg_cols = [cf.sens_bkg for cf in cf_list]
        comb_freqs = [
            cf.comb_freq for cf in cf_list if cf.comb_freq is not None]
        comb_freqs.sort()
        if len(cf_list) == 1:
            # Only one injection; trivial
            cf = cf_list[0]
            factors = cf.values
            factors_counts = cf.values_in_counts
            flags = cf.flags
            injs = np.asarray(injection_cols[0])
            sens_bg = cf.sens_bkg
            comp_cf = cls(
                channel_name, freqs, factors, factors_counts,
                flags, injs, sens_bg=sens_bg, darm_bg=darm_bg,
                comb_freqs=comb_freqs)
            return comp_cf
        ratio_cols = []
        df = cf_list[0].df
        for cf in cf_list:
            ratio = np.zeros(nrows)
            if peak_search_window == 0:
                for i in rows:
                    idx1 = max([0, i - int(5. / df)])
                    idx2 = min([nrows, i + int(5. / df)])
                    mean_inj = cf.sens_inj[idx1: idx2].mean()
                    mean_bg = cf.sens_bkg[idx1: idx2].mean()
                    if mean_inj > mean_bg:
                        ratio[i] = mean_inj / mean_bg
                    else:
                        ratio[i] = 0
            else:
                for i in rows:
                    ratio[i] = max([0, cf.sens_inj[i] / cf.sens_bkg[i]])
            ratio_cols.append(ratio)
        # Quad-sum axes
        axis_channels = list(qsum_axes.keys())
        flag_cols_axes = {}
        ratio_cols_axes = {}
        for axis_channel, axis_cf_list in qsum_axes.items():
            flag_cols_axis = []
            ratio_cols_axis = []
            for cf in axis_cf_list:
                flag_cols_axis.append(cf.flags)
                ratio = np.zeros(nrows)
                if peak_search_window == 0:
                    for i in range(ratio.size):
                        idx1 = max([0, i - int(5. / df)])
                        idx2 = min([nrows, i + int(5. / df)])
                        mean_inj = cf.sens_inj[idx1: idx2].mean()
                        mean_bg = cf.sens_bkg[idx1: idx2].mean()
                        if mean_inj > mean_bg:
                            ratio[i] = mean_inj / mean_bg
                        else:
                            ratio[i] = 0
                else:
                    for i in range(ratio.size):
                        ratio[i] = max([0, cf.sens_inj[i] / cf.sens_bkg[i]])
                ratio_cols_axis.append(ratio)
            flag_cols_axes[axis_channel] = flag_cols_axis
            ratio_cols_axes[axis_channel] = ratio_cols_axis
        # Stack columns in matrices
        matrix_factor = np.column_stack(tuple(factor_cols))
        matrix_factor_counts = np.column_stack(tuple(factor_counts_cols))
        matrix_flag = np.column_stack(tuple(flag_cols))
        matrix_ratio = np.column_stack(tuple(ratio_cols))
        matrix_inj = np.column_stack(tuple(injection_cols))
        matrix_sens_bg = np.column_stack(tuple(sens_bg_cols))
        matrix_flag_axes = {
            key: np.column_stack(tuple(values)) for
            key, values in flag_cols_axes.items()}
        matrix_ratio_axes = {
            key: np.column_stack(tuple(values)) for
            key, values in ratio_cols_axes.items()}
        # Output arrays
        factors = np.zeros(nrows)
        factors_counts = np.zeros(nrows)
        flags = np.array(['No data'] * nrows, dtype=object)
        injs = np.array([None] * nrows, dtype=object)
        ratios = np.zeros(nrows)
        sens_bg = np.zeros(nrows)
        if len(comb_freqs) > 0 and peak_search_window > 0:
            comb_freqs = sorted(set(comb_freqs))
            center_freqs = []
            for f0 in comb_freqs:
                center_freqs += list(np.arange(f0, freqs.max(), f0))
            center_freqs = sorted(set(center_freqs))
            for fc in center_freqs:
                near_fc = (np.abs(freqs - fc) < peak_search_window)
                idx_near_fc = np.flatnonzero(near_fc)
                factor_near = matrix_factor[near_fc, :]
                factor_counts_near = matrix_factor_counts[near_fc, :]
                flag_near = matrix_flag[near_fc, :]
                ratio_near = matrix_ratio[near_fc, :]
                sens_bg_near = matrix_sens_bg[near_fc, :]
                if np.sum(flag_near == "Measured") > 1:
                    mask_nonzero = (
                        (flag_near == "Measured") |
                        (flag_near == "Upper Limit"))
                else:
                    mask_nonzero = (flag_near == "Upper Limit")
                if np.any(mask_nonzero):
                    idx_nonzero = np.argwhere(mask_nonzero)
                    ratio_near_nonzero = ratio_near[mask_nonzero]
                    row_max, col_max = idx_nonzero[ratio_near_nonzero.argmax()]
                    # Deal with quad-sum channels
                    flag_near_axes = {
                        key: matrix_flag_axis[near_fc] for
                        key, matrix_flag_axis in matrix_flag_axes.items()
                    }
                    ratio_near_axes = {
                        key: matrix_ratio_axis[near_fc] for
                        key, matrix_ratio_axis in matrix_ratio_axes.items()
                    }
                    col_max_axes = []
                    for axis_channel in axis_channels:
                        flag_near_axis = flag_near_axes[axis_channel]
                        ratio_near_axis = ratio_near_axes[axis_channel]
                        mask_nonzero_axis = (
                            (flag_near_axis == "Measured") |
                            (flag_near_axis == "Upper Limit"))
                        if np.any(mask_nonzero_axis):
                            idx_nonzero_axis = np.argwhere(mask_nonzero_axis)
                            ratio_near_nonzero_axis = ratio_near_axis[mask_nonzero_axis]
                            _, col_max_axis = idx_nonzero_axis[
                                ratio_near_nonzero_axis.argmax()]
                            col_max_axes.append(col_max_axis)
                    if len(set(col_max_axes)) < len(col_max_axes):
                        # Choose the injection that appears most often in
                        # all axes.
                        col_max_axes = max(
                            set(col_max_axes), key=col_max_axes.count)
                        if np.any(flag_near[row_max, col_max_axes] != 'No data'):
                            col_max = col_max_axes
                    idx = idx_near_fc[row_max]
                    factors[idx] = factor_near[row_max, col_max]
                    factors_counts[idx] = (factor_counts_near[row_max, col_max])
                    flags[idx] = flag_near[row_max, col_max]
                    injs[idx] = injection_names[col_max]
                    ratios[idx] = ratio_near[row_max, col_max]
                    sens_bg[idx] = sens_bg_near[row_max, col_max]
        else:
            for i in rows:
                factor_row = matrix_factor[i, :]
                factor_counts_row = matrix_factor_counts[i, :]
                flag_row = matrix_flag[i, :]
                ratio_row = matrix_ratio[i, :]
                sens_bg_row = matrix_sens_bg[i, :]
                mask_nonzero = (
                    (flag_row == "Measured") |
                    (flag_row == "Upper Limit"))
                if np.any(mask_nonzero):
                    idx_nonzero = np.argwhere(mask_nonzero)
                    ratio_nonzero = ratio_row[mask_nonzero]
                    col_max = idx_nonzero[ratio_nonzero.argmax()][0]
                    factors[i] = factor_row[col_max]
                    factors_counts[i] = factor_counts_row[col_max]
                    flags[i] = flag_row[col_max]
                    injs[i] = injection_names[col_max]
                    ratios[i] = ratio_row[col_max]
                    sens_bg[i] = sens_bg_row[col_max]
        for i, flag in enumerate(flags):
            if flag == 'No data':
                injs[i] = None
        comp_cf = cls(
            channel_name, freqs, factors, factors_counts,
            flags, injs, sens_bg=sens_bg, darm_bg=darm_bg,
            comb_freqs=comb_freqs)
        return comp_cf

    def plot(
            self, filename, in_counts=False, split_injections=True,
            upper_lim=True, freq_min=None, freq_max=None, factor_min=None,
            factor_max=None, fig_width=None, fig_height=None, markers=('o', 'o'),
            markersizes=(3, 3), edgewidths=(0, 0), facecolors=('red', 'blue'),
            edgecolors=('none', 'none')):
        """
        Plot composite coupling function from the data.

        Parameters
        ----------
        filename : str
            Target file name.
        in_counts : bool, optional
            If True, convert coupling function to counts
        split_injections : bool, optional
            If True, split data by injection, using different colors for each.
        upper_lim : bool, optional
            If True, include upper limits in the plot. Otherwise just plot
            measured values.
        freq_min : float, optional
            Minimum frequency (x-axis).
        freq_max : float, optional
            Maximum frequency (x_axis).
        factor_min : float, optional
            Minimum value of coupling factor axis (y-axis).
        factor_max : float, optional
            Maximum value of coupling factor axis (y-axis).
        fig_width : float or int, optional
            Figure width.
        fig_height : float or int, optional
            Figure height.
        """
        # Use factors and sensor units in counts
        if in_counts:
            factors = self.values_in_counts
            coupling_type = ''
            unit = 'Counts'
        else:
            factors = self.values
            coupling_type = self.coupling
            unit = str(self.unit)
            if unit == 'm/s2':
                unit = '(m/s^2)'
            elif unit == 'm/s':
                unit = '(m/s)'

        # PLOT LIMITS
        freqs_plotted = self.freqs[
            (self.flags == 'Measured') |
            (self.flags == 'Upper Limit')]
        if freq_min is None:
            freq_min = max([self.freqs[0], freqs_plotted[0] / 2])
        if freq_max is None:
            freq_max = min([self.freqs[-1], freqs_plotted[-1] * 2])
        factors_plotted = factors[
            (factors > 0) &
            (self.freqs >= freq_min) &
            (self.freqs < freq_max)]
        if factor_min is None:
            factor_min = factors_plotted.min() / 2
        if factor_max is None:
            factor_max = factors_plotted.max() * 2

        # PLOT PROPERTIES
        marker, marker_upper = markers
        ms, ms_upper = markersizes
        ew, ew_upper = edgewidths
        fc, fc_upper = facecolors
        ec, ec_upper = edgecolors

        # CREATE FIGURE
        cplot = CompositePlot(
            fig_width=fig_width, fig_height=fig_height,
            x_min=freq_min, x_max=freq_max,
            y_min=factor_min, y_max=factor_max
        )

        # Y AXIS LABEL
        if '-WFS_' in self.name:
            coupling_type = 'Beam Jitter'
            if in_counts:
                coupling_unit = "[m in DARM / Counts]"
            else:
                coupling_unit = "[m in DARM / beam diameters of jitter]"
        else:
            coupling_unit = '[m / %s]' % unit
        if len(coupling_type) > 20 or len(coupling_unit) > 20:
            ylabel = "%s Coupling\n%s" % (coupling_type, coupling_unit)
        else:
            ylabel = "%s Coupling %s" % (coupling_type, coupling_unit)
        cplot.set_ylabel(ylabel)

        # TITLE
        if 'XYZ' in self.name:
            title = (self.name[:-4].replace('_', ' ') +
                     " (Quadrature sum of X, Y, and Z components)\n"
                     "Composite Coupling Function\n(based on loudest "
                     "injection at each frequency)")
        else:
            title = (self.name.replace('_', ' ') +
                     " - Composite Coupling Function\n(based on loudest "
                     "injection at each frequency)")
        cplot.set_title(title)

        # PLOT DATA
        if split_injections:

            # COLOR MAP
            # Generate & discretize color map for distinguishing injections
            injection_names = self.unique_injections
            if None in injection_names:
                injection_names.remove(None)
            colors_dict = get_colors(injection_names)

            # PLOTTING LOOP
            lgd_patches = []
            lgd_labels = []
            for injection in injection_names:
                # Color by injection
                c = colors_dict[injection]
                lgd_patches.append(mpatches.Patch(color=c))
                lgd_labels.append(injection)
                mask_injection = (
                    (self.injections == injection) & (self.values > 0))
                mask_measured = mask_injection & (self.flags == 'Measured')
                if in_counts:
                    mask_upper = (
                        mask_injection &
                        ((self.flags == 'Upper Limit') |
                         (self.flags == 'Thresholds not met')))
                else:
                    mask_upper = (
                        mask_injection & (self.flags == 'Upper Limit'))
                cplot.plot(
                    self.freqs[mask_measured],
                    factors[mask_measured],
                    marker,
                    markersize=ms,
                    markerfacecolor=tuple(c[:-1]),
                    markeredgewidth=ew,
                    markeredgecolor='k',
                    zorder=2)
                if upper_lim:
                    cplot.plot(
                        self.freqs[mask_upper],
                        factors[mask_upper],
                        marker_upper,
                        markersize=ms_upper,
                        markeredgewidth=ew_upper,
                        markerfacecolor='none',
                        markeredgecolor=tuple(c[:-1]),
                        alpha=0.5,
                        zorder=1)
        else:
            lgd_patches = [mpatches.Patch(color='r')]
            lgd_labels = [
                "Measured Value (Signal seen in both sensor and DARM)"]
            mask_measured = self.flags == 'Measured'
            mask_upper = self.flags == 'Upper Limit'
            mask_null = self.flags == 'Thresholds not met'
            cplot.plot(
                self.freqs[mask_measured],
                factors[mask_measured],
                marker,
                markersize=ms,
                markerfacecolor=fc,
                markeredgewidth=ew,
                zorder=3)
            if upper_lim:
                lgd_patches.append(mpatches.Patch(color='b'))
                lgd_labels.append(
                    "Upper Limit (Signal seen in sensor but not DARM)")
                cplot.plot(
                    self.freqs[mask_upper],
                    factors[mask_upper],
                    marker_upper,
                    markersize=ms_upper,
                    markerfacecolor=fc_upper,
                    markeredgewidth=ew_upper,
                    zorder=2)
                if in_counts:
                    cplot.plot(
                        self.freqs[mask_null],
                        factors[mask_null],
                        marker_upper,
                        markersize=ms_upper,
                        markerfacecolor='cyan',
                        markeredgewidth=ew_upper,
                        zorder=1)
                    lgd_patches.append(mpatches.Patch(color='cyan'))
                    lgd_labels.append(
                        "Thresholds Not Met (Signal not seen " +
                        "in sensor nor DARM)")

        # LEGEND
        cplot.legend(
            patches=lgd_patches, labels=lgd_labels)

        # CAPTION
        if split_injections:
            caption1 = ("CIRCLES represent measured coupling factors, "
                        "i.e. where a signal was seen in both sensor "
                        "and DARM.")
            if 'MAG' in self.name:
                caption2 = ("TRIANGLES represent upper limit coupling "
                            "factors, i.e. where a signal was seen in "
                            "the sensor only.")
            else:
                caption2 = ("TRIANGLES represent upper limit coupling "
                            "factors, i.e. where a signal was not seen "
                            "in DARM.")
            cplot.caption('\n'.join((caption1, caption2)))

        # BANDWIDTH
        cplot.bandwidth(self.df)

        # EXPORT PLOT
        cplot.save(filename)
        cplot.close()
        return

    def ambientplot(
            self, filename, gw_signal='darm', split_injections=True,
            gwinc_data=None, gwinc_label='', darm_data=None,
            freq_min=None, freq_max=None, amb_min=None, amb_max=None,
            fig_width=18, fig_height=12, markers=('o', 'o'),
            markersizes=(3, 3), edgewidths=(0, 0), facecolors=('red', 'blue'),
            edgecolors=('none', 'none')):
        """
        Plot DARM spectrum and composite estimated ambient from the data.

        Parameters
        ----------
        filename : str
            Target file name.
        gw_signal : 'darm' or 'strain', optional
            Treat GW channel as DARM (meters/Hz^(1/2)) or strain (1/Hz^(1/2)).
        split_injections : bool, optional
            If True, split data by injection, using different colors for each.
        gwinc : tuple of arrays, optional
            Frequency and ASD arrays of GWINC estimate, for plotting.
        darm_data: tuple of arrays, optional
            Frequency and ASD arrays of DARM spectrum, for plotting.
        freq_min : float, optional
            Minimum frequency (x-axis).
        freq_max : float, optional
            Maximum frequency (x_axis).
        factor_min : float, optional
            Minimum value of coupling factor axis (y-axis).
        factor_max : float, optional
            Maximum value of coupling factor axis (y-axis).
        fig_w : float or int, optional
            Figure width.
        fig_height : float or int, optional
            Figure height.
        """

        # PLOT PROPERTIES
        marker, marker_upper = markers
        ms, ms_upper = markersizes
        ew, ew_upper = edgewidths
        fc, fc_upper = facecolors
        ec, ec_upper = edgecolors

        # DARM DATA
        if darm_data is None:
            darm_freqs, darm_bg = [self.freqs, self.darm_bkg]
        else:
            darm_freqs, darm_bg = darm_data
        if gw_signal.lower() == 'strain':
            darm_bg = darm_bg / 4000.
            ambients = self.ambients / 4000.
        else:
            ambients = self.ambients

        # PLOT LIMITS
        freqs_plotted = self.freqs[
            (self.flags == 'Measured') |
            (self.flags == 'Upper Limit')]
        if freq_min is None:
            freq_min = max([self.freqs[0], freqs_plotted[0] / 2])
        if freq_max is None:
            freq_max = min([self.freqs[-1], freqs_plotted[-1] * 2])
        ambients_plotted = ambients[
            (ambients > 0) &
            (self.freqs >= freq_min) &
            (self.freqs < freq_max)]
        if amb_min is None:
            amb_min = ambients_plotted.min() / 2
        if amb_max is None:
            amb_max = ambients_plotted.max() * 2

        # CREATE FIGURE
        cplot = CompositePlot(
            fig_width=fig_width, fig_height=fig_height,
            x_min=freq_min, x_max=freq_max,
            y_min=amb_min, y_max=amb_max
        )

        # Y AXIS LABEL
        if gw_signal.lower() == 'darm':
            ylabel = "DARM ASD [m/sqrt(Hz)]"
        else:
            ylabel = "Strain ASD [1/sqrt(Hz)]"
        cplot.set_ylabel(ylabel)

        # TITLE
        if 'XYZ' in self.name:
            title = (self.name.replace('_XYZ', '').replace('_', ' ') +
                     " (Quadrature sum of X, Y, and Z components)"
                     "\nComposite Estimated Ambient")
        else:
            title = (self.name.replace('_', ' ') +
                     " - Composite Estimated Ambient")
        title += ("\n(Coupling factors chosen based on loudest "
                  "injection at each frequency)")
        cplot.set_title(title)

        # PLOT DARM
        darmline1, = cplot.plot(
            darm_freqs, darm_bg, 'k-', lw=.7,
            label="DARM background", zorder=3
        )
        darmline2, = cplot.plot(
            darm_freqs, darm_bg / 10., 'k--', alpha=0.6,
            lw=.7, label="DARM background / 10", zorder=3
        )
        lgd_patches = [darmline1]
        lgd_labels = ["DARM background"]

        # PLOT GWINC
        if gwinc_data is not None:
            gwinc_freqs = gwinc_data[0]
            if gw_signal.lower() == 'strain':
                gwinc_amp = gwinc_data[1] / 4000
            else:
                gwinc_amp = gwinc_data[1]
            gwincline, = cplot.plot(
                gwinc_freqs, gwinc_amp, color='0.5', lw=3, zorder=1)
            lgd_patches.append(gwincline)
            lgd_labels.append(gwinc_label)

        # COLORMAP FOR DIFFERENT INJECTIONS
        injection_names = self.unique_injections
        if None in injection_names:
            injection_names.remove(None)
        colors_dict = get_colors(injection_names)

        # MAIN LOOP FOR PLOTTING AMBIENTS
        if split_injections:
            for injection in injection_names:
                c = colors_dict[injection]
                lgd_patches.append(mpatches.Patch(color=tuple(c[:-1])))
                lgd_labels.append(injection)
                mask_injection = (
                    (self.injections == injection) & (ambients > 0))
                mask_measured = mask_injection & (self.flags == 'Measured')
                mask_upper = mask_injection & (self.flags == 'Upper Limit')
                if np.any(mask_measured):
                    cplot.plot(
                        self.freqs[mask_measured],
                        ambients[mask_measured],
                        marker,
                        markersize=ms,
                        markerfacecolor=tuple(c[:-1]),
                        markeredgewidth=ew,
                        markeredgecolor='k',
                        zorder=4)
                if np.any(mask_upper):
                    cplot.plot(
                        self.freqs[mask_upper],
                        ambients[mask_upper],
                        marker_upper,
                        markersize=ms_upper,
                        markerfacecolor='none',
                        markeredgewidth=ew_upper,
                        markeredgecolor=tuple(c[:-1]),
                        zorder=3)
        else:
            lgd_patches.append(mpatches.Patch(color='r'))
            lgd_labels.append(
                "Measured Value (Signal seen in both sensor and DARM)")
            lgd_patches.append(mpatches.Patch(color='b'))
            lgd_labels.append(
                "Upper Limit (Signal seen in sensor but not DARM)")
            lgd_patches.append(mpatches.Patch(color='c'))
#             lgd_labels.append(
#                 "Thresholds Not Met (Signal not seen in sensor nor DARM)")
            mask_measured = self.flags == 'Measured'
            mask_upper = self.flags == 'Upper Limit'
            cplot.plot(
                self.freqs[mask_measured], ambients[mask_measured],
                marker, markersize=ms, markerfacecolor=fc,
                markeredgewidth=ew, zorder=3)
            cplot.plot(
                self.freqs[mask_upper], ambients[mask_upper],
                marker_upper, markersize=ms_upper, markerfacecolor=fc_upper,
                markeredgewidth=ew_upper, zorder=2
            )

        # LEGEND
        cplot.legend(
            patches=lgd_patches, labels=lgd_labels)

        # CAPTION
        if split_injections:
            caption = (
                "Ambient estimates are made by multiplying coupling "
                "factors by injection-free sensor levels.\nCIRCLES "
                "indicate estimates from measured coupling factors, "
                "i.e. where the injection signal was seen in the "
                "sensor and in DARM.")
            if 'MAG' in self.name:
                caption += (
                    "\nTRIANGLES represent upper limit coupling "
                    "factors, i.e. where a signal was seen in "
                    "the sensor only.")
            else:
                caption += (
                    "\nTRIANGLES represent upper limit coupling "
                    "factors, i.e. where a signal was not seen "
                    "in DARM.")
        else:
            caption = (
                "Ambient estimates are made by multiplying coupling "
                "factors by injection-free sensor levels.")
        cplot.caption(caption)

        # BANDWIDTH
        cplot.bandwidth(self.df)

        # EXPORT PLOT
        cplot.save(filename)
        cplot.close()
        return

    def to_csv(self, filename):
        """
        Save data to a csv file.

        Parameters
        ----------
        filename : str
            Target file name.
        """

        with open(filename, 'w') as file:
            comb_freqs_str = []
            for f in self.comb_freqs:
                s = '{:.2f}'.format(f)
                if s not in comb_freqs_str:
                    comb_freqs_str.append(s)
            file.write('#combfreqs:' + ','.join(comb_freqs_str) + '\n')
            file.write('frequency,factor,factor_counts,flag,ambient,darm\n')
            for i in range(len(self.freqs)):
                row = [
                    '{:.2f}'.format(self.freqs[i]),
                    '{:.2e}'.format(self.values[i]),
                    '{:.2e}'.format(self.values_in_counts[i]),
                    self.flags[i],
                    '{:.2e}'.format(self.ambients[i]),
                    '{:.2e}'.format(self.darm_bkg[i])
                ]
                line = ','.join(row) + '\n'
                file.write(line)
        return
