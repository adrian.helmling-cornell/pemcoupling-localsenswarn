"""
Tools for computing and plotting site-wide coupling functions.
"""

import numpy as np
import pandas as pd
import matplotlib
# Set matplotlib backend for saving figures
if matplotlib.get_backend().lower() != 'agg':
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from textwrap import wrap
import logging
# pemcoupling modules
from ..pemchannel import PEMChannel
from ..utils import pem_sort
from .composite_utils import get_colors

# Fix usetex parameter changed by gwpy
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'

# ===========================================================================

SENSOR_LOCATIONS = [
    'CS_ACC_PSL_',
    'CS_ACC_ISCT1_',
    'CS_ACC_IOT1_',
    'CS_ACC_ISCT6_',
    'CS_ACC_SQZT6_',
    'CS_ACC_BSC',
    'EX_ACC_BSC4_',
    'EY_ACC_BSC5_',
    'EX_ACC_BSC9_',
    'EY_ACC_BSC10_',
    'CS_ACC_HAM1_',
    'CS_ACC_HAM2_',
    'CS_ACC_HAM3_',
    'CS_ACC_HAM4_',
    'CS_ACC_HAM5_',
    'CS_ACC_HAM6_',
    'CS_ACC_HAM6VAC_SEPTUM',
    'WFS_'
]

SENSOR_UNITS = {
    'ACC'  : 'm',
    'HPI'  : 'm',
    'ISI'  : 'm',
    'MAG'  : 'T',
    'MIC'  : 'Pa',
    'RADIO': 'ADC',
    'SEIS' : 'm'
}

DEFAULT_FIG_WIDTH = 8
DEFAULT_FIG_HEIGHT = 6

COUPLING_AXIS_X0 = 0.1
COUPLING_AXIS_X1 = 0.98
COUPLING_AXIS_Y0 = 0.25
COUPLING_AXIS_Y1 = 0.90
COUPLING_AXIS_POS = [
    COUPLING_AXIS_X0,
    COUPLING_AXIS_Y0,
    COUPLING_AXIS_X1 - COUPLING_AXIS_X0,
    COUPLING_AXIS_Y1 - COUPLING_AXIS_Y0
]
AMBIENT_AXIS_X0 = 0.1
AMBIENT_AXIS_X1 = 0.98
AMBIENT_AXIS_Y0 = 0.3
AMBIENT_AXIS_Y1 = 0.90
AMBIENT_AXIS_POS = [
    AMBIENT_AXIS_X0,
    AMBIENT_AXIS_Y0,
    AMBIENT_AXIS_X1 - AMBIENT_AXIS_X0,
    AMBIENT_AXIS_Y1 - AMBIENT_AXIS_Y0
]

COMPACT_AXIS_X0 = 0.1
COMPACT_AXIS_X1 = 0.98
COMPACT_AXIS_Y0 = 0.1
COMPACT_AXIS_Y1 = 0.9
COMPACT_AXIS_POS = [
    COMPACT_AXIS_X0,
    COMPACT_AXIS_Y0,
    COMPACT_AXIS_X1 - COMPACT_AXIS_X0,
    COMPACT_AXIS_Y1 - COMPACT_AXIS_Y0
]

DEFAULT_AXIS_POS = {
    'full': {
        'coupling': COUPLING_AXIS_POS,
        'ambient': AMBIENT_AXIS_POS
    },
    'compact': {
        'coupling': COMPACT_AXIS_POS,
        'ambient': COMPACT_AXIS_POS
    }
}

DEFAULT_PLOT_PARAMS = {
    'full': {
        'xlabel_fontsize'  : 25,
        'ylabel_fontsize'  : 25,
        'ylabel_pad'       : 30,
        'title_y'          : 1.01,
        'title_fontsize'   : 30,
        'tick_labelsize'   : 25,
        'caption_width'    : 160,
        'lgd_fontsize'     : 14,
        'lgd_bbox'         : (0.5, -0.13),
        'lgd_loc'          : 'upper center',
        'lgd_height'       : 0.18,
        'lgd_alpha'        : 1
    },
    'compact': {
        'xlabel_fontsize'  : 25,
        'ylabel_fontsize'  : 25,
        'ylabel_pad'       : 30,
        'title_y'          : 1.01,
        'title_fontsize'   : 30,
        'tick_labelsize'   : 25,
        'caption_width'    : 160,
        'lgd_fontsize'     : 20,
        'lgd_bbox'         : (1, 1),
        'lgd_loc'          : 'upper right',
        'lgd_height'       : 0.2,
        'lgd_alpha'        : 1
    }
}

# ===========================================================================


def max_coupling_function(
        freqs, factor_df, flag_df, freq_lines={}, peak_search_window=0,
        measured_value_threshold=None,
        sensor_locations=SENSOR_LOCATIONS):
    """
    Determine maximum coupling function.

    Parameters
    ----------
    freqs : array-like
        Frequency array.
    factor_df : pandas.DataFrame
        Dataframe where each column is a coupling function for a channel.
    flag_df : pandas.DataFrame
        Corresponding flags for the coupling factors in factor_df.

    Returns
    -------
    max_factor_df : pandas.DataFrame
        Maximum coupling function with corresponding frequencies, flags, and
        channels.
    """

    channels = list(factor_df.columns)
    nrows = len(freqs)
    factors_out = np.zeros(nrows)
    flags_out = np.array([None] * nrows, dtype=object)
    channels_out = np.array([None] * nrows, dtype=object)
    for i, freq in enumerate(freqs):
        factor_row = factor_df.iloc[i].values
        flag_row = flag_df.iloc[i].values
        measured = (flag_row == "Measured")
        upperlim = (flag_row == "Upper Limit")
        if np.any(measured) and not np.any(upperlim):
            mask = measured
        elif np.any(measured) and np.any(upperlim):
            measured_max = factor_row[measured].max()
            upperlim_max = factor_row[upperlim].max()
            if measured_value_threshold is not None:
                if measured_max > measured_value_threshold * upperlim_max:
                    mask = measured
                else:
                    mask = upperlim
            else:
                mask = measured
        elif np.any(upperlim):
            mask = upperlim
        else:
            continue
        idx = np.flatnonzero(mask)
        values_masked = factor_row[mask]
        col_max = idx[values_masked.argmax()]
        factors_out[i] = factor_row[col_max]
        flags_out[i] = flag_row[col_max]
        channels_out[i] = channels[col_max]
    max_factor_dict = {
        'frequency': freqs,
        'factor': factors_out,
        'flag': flags_out,
        'channel': channels_out
    }
    max_factor_df = pd.DataFrame.from_dict(max_factor_dict)
    if len(freq_lines) > 0 and peak_search_window > 0:
        fmin = min(max_factor_df['frequency'])
        fmax = max(max_factor_df['frequency'])
        for idx, row in max_factor_df.iterrows():
            freq = row['frequency']
            factor = row['factor']
            channel = row['channel']
            combs = freq_lines.get(channel, [])
            if factor > 0 and len(combs) > 0:
                all_lines = []
                for f0 in combs:
                    all_lines += list(np.arange(f0, fmax, f0))
                f_nearest = min(all_lines, key=lambda x:abs(x - freq))
                if abs(freq - f_nearest) < peak_search_window / 0.5:
                    f1 = max([fmin, freq - peak_search_window])
                    f2 = min([fmax, freq + peak_search_window])
                    i1 = np.argmax(max_factor_df['frequency'] > f1)
                    i2 = np.argmax(max_factor_df['frequency'] > f2)
                    if i2 < i1:
                        i2 = max(max_factor_df.index)
                    loc_factors = max_factor_df['factor'][i1: i2]
                    loc_flags = max_factor_df['flag'][i1: i2]
                    if 'Measured' in loc_flags.values:
                        loc_factors_measured = loc_factors[loc_flags == 'Measured']
                        loc_max_idx = loc_factors_measured.idxmax()
                    else:
                        loc_max_idx = loc_factors.idxmax()
                    if idx != loc_max_idx:
                        max_factor_df.loc[idx, 'factor'] = 0
                        max_factor_df.loc[idx, 'flag'] = None
                        max_factor_df.loc[idx, 'channel'] = None
    max_factor_df = max_factor_df[pd.notnull(max_factor_df['channel'])]
    channels = sorted(set(max_factor_df['channel'].values))
    replace_dict = {}
    if len(channels) > 15:
        # Cluster channel names by location if there are too many channels
        # to plot
        for c in channels:
            for x in sensor_locations:
                if x in c:
                    replace_dict[c] = x.rstrip('_')
    max_factor_df['location'] = max_factor_df['channel'].copy()
    for k, v in replace_dict.items():
        max_factor_df['location'].replace(k, v, inplace=True)
    return max_factor_df


def max_estimated_ambient(
        freqs, amb_df, flag_df, freq_lines={}, peak_search_window=None,
        measured_value_threshold=None,
        sensor_locations=SENSOR_LOCATIONS):
    """
    Determine maximum estimated ambient.

    Parameters
    ----------
    freqs : array-like
        Frequency array.
    amb_df : pandas.DataFrame
        Dataframe where each column is an estimated ambient for a channel.
    flag_df : pandas.DataFrame
        Corresponding flags for the estimated ambients in amb_df.

    Returns
    -------
    max_amb_df : pandas.DataFrame
        Maximum ambient with corresponding frequencies, flags, and channels.
    """

    max_amb_df = max_coupling_function(
        freqs, amb_df, flag_df,
        freq_lines=freq_lines,
        peak_search_window=peak_search_window,
        measured_value_threshold=measured_value_threshold,
        sensor_locations=sensor_locations
    )
    max_amb_df.rename(columns={'factor': 'amb'}, inplace=True)
    return max_amb_df


# ===========================================================================


class SummaryPlot(object):
    def __init__(
            self,
            fig_width=DEFAULT_FIG_WIDTH,
            fig_height=DEFAULT_FIG_HEIGHT,
            ax_pos=None, x_min=None,
            x_max=None,
            y_min=None,
            y_max=None,
            markers=('o', '^'),
            markersizes=(4.0, 0.6),
            edgewidths=(0.5, 0.7),
            facecolors=('red', 'blue'),
            plot_type='coupling',
            plot_style='full'):
        # Plot properties
        self.plot_style = plot_style
        self.ax_pos = DEFAULT_AXIS_POS[plot_style][plot_type]
        self.params = DEFAULT_PLOT_PARAMS[plot_style]
        self.fig = plt.figure(figsize=(fig_width, fig_height))
        self.ax = self.fig.add_subplot(111)
        self.ax.set_position(self.ax_pos)
        # Axes scaling
        if x_min is not None and x_max is not None:
            self.xlim = (x_min, x_max)
        else:
            self.xlim = None
        if y_min is not None and y_max is not None:
            self.ylim = (y_min, y_max)
        else:
            self.ylim = None
        self.ax.set_xlim(self.xlim)
        self.ax.set_ylim(self.ylim)
        if self.xlim[1] / self.xlim[0] > 100:
            self.ax.set_xscale('log', nonpositive='clip')
        if self.ylim[1] / self.ylim[0] > 100:
            self.ax.set_yscale('log', nonpositive='clip')
        self.ax.autoscale(False)
        self.ax.grid(
            b=True, which='major', color='0.0', linestyle=':', zorder=1)
        self.ax.grid(
            b=True, which='minor', color='0.6', linestyle=':', zorder=1)
        self.ax.minorticks_on()
        # Tick label fontsizes
        for tick in self.ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(self.params['tick_labelsize'])
        for tick in self.ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(self.params['tick_labelsize'])
        # Frequency axis label
        self.ax.set_xlabel(
            'Frequency [Hz]', size=self.params['xlabel_fontsize'])
        # Plot parameters
        self.markers = markers
        self.markersizes = markersizes
        self.edgewidths = edgewidths
        self.facecolors = facecolors
        # Legend patches
        self.lgd_patches = []
        self.lgd_labels = []

    def set_colors(self, channels):
        self.colors = get_colors(channels)

    def add_data(self, channel, x, y, short_names=True, flag=0, zorder=0):
        patch_color = self.colors[channel]
        if short_names and '-' in channel:
            channel = channel[channel.index('-') + 1: ]
        if 'QUAD_SUM' in channel:
            patch_label = channel.replace('QUAD_SUM', '').replace('_DQ', '')
        else:
            patch_label = channel.replace('_DQ', '')
        if patch_label not in self.lgd_labels:
            self.lgd_patches.append(
                mpatches.Patch(color=patch_color, label=patch_label))
            self.lgd_labels.append(patch_label)
        if flag == 0:
            self.ax.plot(
                x, y, self.markers[flag],
                markersize=self.markersizes[flag],
                color=patch_color,
                markeredgewidth=self.edgewidths[flag],
                markeredgecolor='k',
                label=channel, zorder=zorder)
        else:
            self.ax.plot(
                x, y, self.markers[flag],
                markersize=self.markersizes[flag],
                color='none',
                markeredgewidth=self.edgewidths[flag],
                markeredgecolor=patch_color,
                label=channel,
                alpha=0.5,
                zorder=zorder)
    
    def add_data_combined(self, x, y, add_to_lgd=False, flag=0, zorder=0):
        patch_color = self.facecolors[flag]
        patch_label = ('Measured Value' if flag == 0 else 'Upper Limit')
        if patch_label not in self.lgd_labels:
            self.lgd_patches.append(
                    mpatches.Patch(color=patch_color, label=patch_label))
            self.lgd_labels.append(patch_label)
        self.ax.plot(
            x, y, self.markers[flag],
            markersize=self.markersizes[flag],
            color=patch_color,
            markeredgewidth=self.edgewidths[flag],
            markeredgecolor='k',
            label=patch_label, zorder=zorder)

    def add_line(
            self, x, y, fmt='', label=None, color='0',
            lw=3, zorder=0, **kwargs):
        if label is not None:
            self.lgd_patches.append(
                mpatches.Patch(color=color, label=label))
            self.lgd_labels.append(label)
        self.ax.plot(x, y, fmt, color=color, lw=lw, zorder=1, **kwargs)

    def set_ylabel(self, ylabel, labelpad=None, size=None):
        if labelpad is None:
            labelpad = self.params['ylabel_pad']
        if size is None:
            size = self.params['ylabel_fontsize']
        if '\n' in ylabel:
            size *= 0.85
        self.ax.set_ylabel(
            ylabel, ha='center', va='center',
            labelpad=labelpad, size=size)

    def set_title(self, title, size=None):
        if size is None:
            size = self.params['title_fontsize']
        self.ax.set_title(title, size=size)

    def caption(self, caption, size=None, width=None):
        if size is None:
            size = self.params['lgd_fontsize']
        if width is None:
            width = self.params['caption_width']
        caption = '\n'.join(wrap(caption, width))
        caption_pos = 0.01 - self.ax_pos[1] / self.ax_pos[3]
        self.ax.text(
            0.5, caption_pos, caption,
            size=size, va='bottom', ha='center',
            transform=self.ax.transAxes)

    def bandwidth(self, bw, fontsize=None):
        if fontsize is None:
            fontsize = self.params['lgd_fontsize']
        self.ax.text(
            1, -0.1, 'Bin width: %1.3f Hz' % bw,
            ha='right', va='bottom', size=fontsize,
            transform=self.ax.transAxes)

    def legend(
            self, size=None, bbox_to_anchor=None,
            loc=None, alpha=None):
        if size is None:
            size = self.params['lgd_fontsize']
        if bbox_to_anchor is None:
            bbox_to_anchor = self.params['lgd_bbox']
        if loc is None:
            loc = self.params['lgd_loc']
        if alpha is None:
            alpha = self.params['lgd_alpha']
        ncol = 1
        lgd = self.ax.legend(
            self.lgd_patches,
            self.lgd_labels,
            prop={'size': size},
            bbox_to_anchor=bbox_to_anchor,
            loc=loc,
            borderaxespad=0.,
            ncol=ncol)
        lgd_w, lgd_h = self.get_lgd_dim(lgd)
        ax_w = self.ax.get_window_extent().width
        ax_h = self.ax.get_window_extent().height
        max_w = ax_w
        max_h = self.params['lgd_height'] * ax_h
        while (lgd_w > max_w or lgd_h > max_h):
            if lgd_h > self.params['lgd_height'] * ax_h:
                ncol += 1
            else:
                size *= 0.99
                ncol = max(1, ncol - 1)
            lgd = self.ax.legend(
                self.lgd_patches,
                self.lgd_labels,
                prop={'size': size},
                bbox_to_anchor=bbox_to_anchor,
                loc=loc,
                borderaxespad=0.,
                ncol=ncol)
            lgd_w, lgd_h = self.get_lgd_dim(lgd)
        lgd.get_frame().set_alpha(alpha)

    def get_lgd_dim(self, lgd):
        self.fig.canvas.draw()
        lgd_extent = lgd.get_window_extent()
        return lgd_extent.width, lgd_extent.height

    def save(self, filename):
        self.fig.savefig(filename, dpi=self.fig.get_dpi() * 2)


def get_params_from_channels(channels):
    ifo, station, summary_type = (None, None, None)
    ifo_list, station_list, sensor_list = [[], [], []]
    for c in channels:
        channel = PEMChannel(c)
        ifo_list.append(channel.ifo)
        station_list.append(channel.station)
        sensor_list.append(channel.sensor)
    if len(set(ifo_list)) == 1:
        ifo = ifo_list[0]
    if len(set(station_list)) == 1:
        station = station_list[0]
    sensor = max(set(sensor_list), key=sensor_list.count)
    if sensor in ['ACC', 'MIC']:
        summary_type = 'Vibrational'
    elif sensor in ['MAG', 'MAINSMON']:
        summary_type = 'Magnetic'
    elif sensor == 'WFS':
        summary_type = 'Jitter'
    else:
        raise Exception(
            'Could not determine injection type from channels '
            'provided.')
    return ifo, station, summary_type


def summary_plot_title(ifo=None, station=None, summary_type=None, plot=None):
    station_dict = {
        'CS': 'Corner Station',
        'EX': 'End Station X',
        'EY': 'End Station Y'}
    title = ""
    if ifo or station or summary_type:
        if ifo or summary_type:
            if ifo:
                title += ifo + " "
            if summary_type:
                title += summary_type + " "
            title += "- "
        if station:
            if station in station_dict.keys():
                title += station_dict[station] + " "
            else:
                title += station + " "
    else:
        title = 'Summary '
    if plot == 'coupling':
        title += 'Coupling Function\n(Highest coupling factor at each frequency across all channels)'
    elif plot == 'ambient':
        title += 'Estimated Ambient\n(Highest estimated ambient at each frequency across all channels)'
    return title


def split_data_by_flag(data, locations, value='factor'):
    cols = ['frequency', value]
    measured, upper, null = ({}, {}, {})
    for location in locations:
        measured_data = data[
            (data['flag'] == 'Measured') &
            (data['location'] == location)][cols]
        upper_data = data[
            (data['flag'] == 'Upper Limit') &
            (data['location'] == location)][cols]
        null_data = data[
            (data['flag'] == 'Thresholds not met') &
            (data['location'] == location)][cols]
        if measured_data.shape[0] > 0:
            measured[location] = np.asarray(measured_data).T
        if upper_data.shape[0] > 0:
            upper[location] = np.asarray(upper_data).T
        if null_data.shape[0] > 0:
            null[location] = np.asarray(null_data).T
    return measured, upper, null


def get_units(channels, units_dict=SENSOR_UNITS):
    units = []
    for key, val in units_dict.items():
        if any(key in c for c in channels):
            units.append(val)
    return units


# ===========================================================================


def plot_summary_coupfunc(
        data,
        filepath,
        split_channels=False,
        upper_lim=True,
        bw=None,
        x_min=None,
        x_max=None,
        y_min=None,
        y_max=None,
        markers=('o', '^'),
        markersizes=(4.0, 0.6),
        edgewidths=(0.5, 0.7),
        facecolors=('red', 'blue'),
        edgecolors=(None, None),
        fig_width=8,
        fig_height=6,
        title=None,
        plot_style='full'
    ):
    # Organize data for plotting
    channels = sorted(set(data['channel']))
    channels = pem_sort(channels)
    locations = sorted(set(data['location']))
    locations = pem_sort(locations)
    freqs = data['frequency'].values
    factors = data['factor'].values
    flags = data['flag'].values
    # Plot limits
    if upper_lim:
        if y_min is None:
            y_min = factors[factors > 1e-99].min() / 10
        if y_max is None:
            y_max = factors.max() * 10
        freqs_plotted = freqs[flags != 'No data']
    else:
        measured_y = data['factor'][flags == 'Measured']
        if y_min is None:
            y_min = measured_y[measured_y > 1e-99].min() / 10
        if y_max is None:
            y_max = measured_y.max() * 10
        freqs_plotted = freqs[flags == 'Measured']
    if x_min is None:
        x_min = max([freqs[0], freqs_plotted[0] / 1.2])
    if x_max is None:
        x_max = min([freqs[-1], freqs_plotted[-1] * 1.2])
    # Create summary plot
    summary_plot = SummaryPlot(
        fig_width=fig_width,
        fig_height=fig_height,
        x_min=x_min,
        x_max=x_max,
        y_min=y_min,
        y_max=y_max,
        markers=markers,
        markersizes=markersizes,
        edgewidths=edgewidths,
        plot_type='coupling',
        plot_style=plot_style)
    summary_plot.set_colors(locations)
    # Add data to plots
    measured_data, upper_data, null_data = split_data_by_flag(
        data, locations, value='factor')
    num_pts_per_location = []
    for loc in locations:
        num_pts = 0
        if loc in measured_data.keys():
            num_pts += len(measured_data[loc][0])
        if loc in upper_data.keys():
            num_pts += len(upper_data[loc][0])
        num_pts_per_location.append(num_pts)
    sort_by_num_pts = np.argsort(num_pts_per_location)[::-1]
    locations_by_num_pts = [locations[i] for i in sort_by_num_pts]
    zorder_dict = {
        loc: 3 * i for i, loc in enumerate(locations_by_num_pts)}
    for loc in locations:
        zorder = zorder_dict[loc]
        if loc in measured_data.keys():
            if split_channels:
                summary_plot.add_data(
                    loc,
                    measured_data[loc][0],
                    measured_data[loc][1],
                    flag=0, zorder=zorder + 3)
            else:
                summary_plot.add_data_combined(
                    measured_data[loc][0],
                    measured_data[loc][1],
                    flag=0, zorder=3)
        if upper_lim and loc in upper_data.keys():
            if split_channels:
                summary_plot.add_data(
                    loc,
                    upper_data[loc][0],
                    upper_data[loc][1],
                    flag=1, zorder=zorder + 2)
            else:
                summary_plot.add_data_combined(
                    upper_data[loc][0],
                    upper_data[loc][1],
                    flag=1, zorder=2)
        if upper_lim and loc in null_data.keys():
            if split_channels:
                summary_plot.add_data(
                    loc,
                    null_data[loc][0],
                    null_data[loc][1],
                    flag=1, zorder=zorder + 1)
            else:
                summary_plot.add_data_combined(
                    null_data[loc][0],
                    null_data[loc][1],
                    flag=1, zorder=1)
    # Customize plot
    units = get_units(channels)
    if type(units) != list:
        units = [units]
    if len(units) > 1:
        units_str = ' or '.join(['[m/{}]'.format(u) for u in units])
        ylabel = 'Coupling Function \n' + units_str
    elif len(units) == 1:
        ylabel = 'Coupling Function [m/{}]'.format(units[0])
    else:
        ylabel = 'Coupling Function [m/Counts]'
    summary_plot.set_ylabel(ylabel)
    summary_plot.set_title(title)
    summary_plot.legend(size=20)
    summary_plot.bandwidth(bw)
    if plot_style == 'full':
        # Caption below legend
        caption = (
            "CIRCLES represent measured coupling factors, i.e. "
            "where a signal was seen in both sensor and DARM. "
            "TRIANGLES represent upper limit coupling factors, i.e. "
            "where a signal was seen in the sensor but not in DARM.")
        summary_plot.caption(caption)
    # Save plot
    summary_plot.save(filepath)
    return


def plot_summary_ambient(
        data, filepath,
        split_channels=False,
        upper_lim=True,
        gw_signal='darm',
        darm_data=None,
        gwinc_data=None,
        gwinc_label='',
        bw=None,
        x_min=None,
        x_max=None,
        y_min=None,
        y_max=None,
        markers=('o', '^'),
        markersizes=(4.0, 0.6),
        edgewidths=(0.5, 0.7),
        facecolors=('red', 'blue'),
        edgecolors=(None, None),
        fig_width=8,
        fig_height=6,
        title=None,
        plot_style='full'
    ):
    # Organize data for plotting
    channels = sorted(set(data['channel']))
    channels = pem_sort(channels)
    locations = sorted(set(data['location']))
    locations = pem_sort(locations)
    if darm_data is not None:
        darm_freqs = darm_data[0]
        if gw_signal == 'strain':
            darm_values = darm_data[1] / 4000
        else:
            darm_values = darm_data[1]
    if gwinc_data is not None:
        gwinc_freqs = gwinc_data[0]
        if gw_signal == 'strain':
            gwinc_values = gwinc_data[1] / 4000
        else:
            gwinc_values = gwinc_data[1]
    if gw_signal == 'strain':
        ambs = data['amb'].values / 4000
    else:
        ambs = data['amb'].values
    freqs = data['frequency'].values
    flags = data['flag'].values
    # Plot limits
    if upper_lim:
        all_ambs = ambs
        freqs_plotted = freqs[flags != 'No data']
    else:
        all_ambs = ambs[flags == 'Measured']
        freqs_plotted = freqs[flags == 'Measured']
    if x_min is None:
        x_min = max([freqs[0], freqs_plotted[0] / 1.2])
    if x_max is None:
        x_max = min([freqs[-1], freqs_plotted[-1] * 1.2])
    ambs_plotted = all_ambs[all_ambs > 1e-99]
    if y_min is None:
        y_min = 10 ** np.floor(
            np.log10(ambs_plotted.min()))
    if y_max is None:
        y_max = 10 ** (np.ceil(
            np.log10(ambs_plotted.max())) + 1)
    # Create summary plot
    summary_plot = SummaryPlot(
        fig_width=fig_width,
        fig_height=fig_height,
        x_min=x_min,
        x_max=x_max,
        y_min=y_min,
        y_max=y_max,
        markers=markers,
        markersizes=markersizes,
        edgewidths=edgewidths,
        plot_type='ambient',
        plot_style=plot_style)
    summary_plot.set_colors(locations)
    # Add spectra to plots
    if gwinc_data is not None:
        summary_plot.add_line(
            gwinc_freqs, gwinc_values, label=gwinc_label,
            color='0.5', lw=3, zorder=1)
    if darm_data is not None:
        summary_plot.add_line(
            darm_freqs, darm_values, label='DARM background',
            color='0', lw=1, zorder=2)
        summary_plot.add_line(
            darm_freqs, darm_values / 10.,
            color='0', lw=0.7, ls='--',
            alpha=0.6, zorder=2)
    # Add data to plots
    measured_data, upper_data, null_data = split_data_by_flag(
        data, locations, value='amb')
    num_pts_per_location = []
    for loc in locations:
        num_pts = 0
        if loc in measured_data.keys():
            num_pts += len(measured_data[loc][0])
        if loc in upper_data.keys():
            num_pts += len(upper_data[loc][0])
        num_pts_per_location.append(num_pts)
    sort_by_num_pts = np.argsort(num_pts_per_location)[::-1]
    locations_by_num_pts = [locations[i] for i in sort_by_num_pts]
    zorder_dict = {
        loc: 3 * i for i, loc in enumerate(locations_by_num_pts)}
    for loc in locations:
        zorder = zorder_dict[loc]
        if loc in measured_data.keys():
            x, y = measured_data[loc]
            if gw_signal == 'strain':
                y /= 4000
            if split_channels:
                summary_plot.add_data(
                    loc, x, y, flag=0, zorder=zorder+2)
            else:
                summary_plot.add_data_combined(
                    x, y, flag=0, zorder=zorder+2)
        if upper_lim and loc in upper_data.keys():
            x, y = upper_data[loc]
            if gw_signal == 'strain':
                y /= 4000
            if split_channels:
                summary_plot.add_data(
                    loc, x, y, flag=1, zorder=zorder+1)
            else:
                summary_plot.add_data_combined(
                    x, y, flag=1, zorder=zorder+1)
        if upper_lim and loc in null_data.keys():
            x, y = null_data[loc]
            if gw_signal == 'strain':
                y /= 4000
            if split_channels:
                summary_plot.add_data(
                    loc, x, y, flag=1, zorder=zorder)
            else:
                summary_plot.add_data_combined(
                    x, y, flag=1, zorder=zorder)
    # Customize plot
    if gw_signal == 'strain':
        summary_plot.set_ylabel('Strain ASD [1/sqrt(Hz)]')
    else:
        summary_plot.set_ylabel('DARM ASD [m/sqrt(Hz)]')
    summary_plot.set_title(title)
    summary_plot.legend()
    summary_plot.bandwidth(bw)
    # Caption below legend
    if plot_style == 'full':
        caption = (
            "Ambient estimates are made by multiplying coupling factors "
            "by injection-free sensor levels. CIRCLES indicate estimates "
            "from measured coupling factors, i.e. where the injection "
            "signal was seen in the sensor and in DARM. TRIANGLES represent "
            "upper limit coupling factors, i.e. where a signal was seen in "
            "the sensor but not in DARM. For some channels, at certain "
            "frequencies the ambient estimates are upper limits because "
            "the ambient level is below the sensor noise floor.")
        summary_plot.caption(caption)
    # Save plot
    summary_plot.save(filepath)
    return
