"""
Coupling function calculor
"""

import sys
import os
import time
import logging
import traceback
import shutil
import numpy as np

# pemcoupling modules
from . import utils as pem_utils
from . import plot as pem_plot
from . import parse, preprocess, html
from .injection import Injection
from .pemchannel import PEMChannel
from .ratioplots import ratio_plots
from .postprocess.composite import main as composite_main


def main(args):
    """
    Main script for calculating PEM coupling functions.
    """

    timestamp = time.time()
    verbose = args.verbose

    # CONFIG PARSING
    # Read config file into dictionary
    config_dict = parse.parse_config(args.config)
    if len(config_dict) == 0:
        err_msg = "config parsing failed"
        logging.error(err_msg)
        raise ValueError(err_msg)
    # Assign converted sub-dictionaries to separate dictionaries
    logging.info("Separating config dictionary by section.")
    darm_cfg = config_dict['darm']
    plot_cfg = config_dict['coupling function plot']
    ratio_cfg = config_dict['ratio plot']
    comp_cfg = config_dict['composite coupling function']
    arg_dict = {k: v for k, v in vars(args).items() if v}
    # Output directory
    out_dir = arg_dict.get('output', os.getcwd())

    # GET TIMES
    logging.info("Creating injection table.")
    search_injections = args.search_injections
    if args.times is not None:
        logging.info("Getting times from command line input.")
        try:
            t_bkg, t_inj = pem_utils.get_times_from_args(args.times)
        except Exception as err_msg:
            logging.error(err_msg)
            raise
        injection_name = (
            'injection_{}_{}'.format(t_bkg, t_inj))
        injection_table = [[injection_name, t_bkg, t_inj]]
    elif args.dtt:
        logging.info("Reading DTT files for times.")
        injection_table = []
        for dtt in args.dtt:
            injection_name = os.path.splitext(os.path.basename(dtt))[0]
            if pem_utils.search(search_injections, injection_name):
                try:
                    t_bkg, t_inj = pem_utils.get_times_from_DTT(dtt)
                except Exception as err_msg:
                    logging.warning(err_msg)
                    continue
                injection_table.append([injection_name, t_bkg, t_inj])
        logging.info("Finished reading DTTs.")
    elif args.injection_list is not None:
        logging.info("Reading injection names/times file.")
        try:
            injection_table = pem_utils.get_times_from_txt(args.injection_list)
        except Exception as err_msg:
            logging.error(err_msg)
            raise
        logging.info("Finished reading injection names/times file.")
        # Get injections (rows of injection_table) that match the
        # given injection_search option
        injection_table = [
            row for row in injection_table if
            pem_utils.search(search_injections, row[0])]
    else:
        err_msg = "No times or DTTs given."
        logging.error(err_msg)
        raise ValueError(err_msg)
    if len(injection_table) > 0:
        if args.times is None:
            print("\n{} injection(s) found:".format(len(injection_table)))
            for injection_name, _, _ in injection_table:
                print(injection_name)
    else:
        err_msg = "No injections to process."
        logging.error(err_msg)
        raise RuntimeError(err_msg)

    # Final list of channels for post-processing
    composite_channels = []
    composite_injections = []
    # Collect links and channel names for HTML pages
    sub_dirs = {}
    channel_info = {}
    failed_channels = {}
    saturated_channels = {}

    # LOOP OVER SECTIONS
    for section in args.section:
        if len(args.section) > 1:
            print("\n" + "*" * 20)
        print("\nAnalyzing {} channels".format(section))
        try:
            sect_cfg = config_dict[section]
        except:
            logging.warning("invalid config section " + args.section)
            continue
        logging.info("Handling conflicts between config options "
                     "and command line options.")

        # OVERRIDE CONFIG OPTIONS WHERE APPLICABLE
        # Smoothing
        if args.smoothing:
            smooth_cfg = {
                'smoothing': args.smoothing,
                'smoothing_log': sect_cfg['smoothing_log']}
        else:
            smooth_cfg = {
                'smoothing': sect_cfg['smoothing'],
                'smoothing_log': sect_cfg['smoothing_log']}
        # Gating
        gate_darm = args.gate
        # FFT parameters
        fft_avg = arg_dict.get(
            'fft_avg', sect_cfg.get('fft_avg'))
        duration = arg_dict.get(
            'duration', sect_cfg.get('duration'))
        bin_width = arg_dict.get(
            'bin_width', sect_cfg.get('bin_width'))
        overlap_pct = arg_dict.get(
            'overlap_pct', sect_cfg.get('overlap_pct'))
        # Reject saturated channels
        ignore_saturated = arg_dict.get(
            'ignore_saturated', sect_cfg.get('ignore_saturated'))
        # Thresholds
        darm_threshold = arg_dict.get(
            'darm_threshold', sect_cfg.get('darm_threshold'))
        sensor_threshold = arg_dict.get(
            'sensor_threshold', sect_cfg.get('sensor_threshold'))
        sensor_threshold_autoscale = arg_dict.get(
            'sensor_threshold_autoscale',
            sect_cfg.get('sensor_threshold_autoscale'))
        peak_search_window = sect_cfg.get('peak_search_window')
        slope_threshold = arg_dict.get('slope_threshold')
        # Injection types and frequency ranges
        broadband_freqs_custom = None
        comb_freq_custom = None
        line_freq_custom = None
        if args.comb:
            injection_type = 'comb'
            if type(args.comb) == float:
                comb_freq_custom = args.comb
            else:
                comb_freq_custom = sect_cfg.get('comb_freq')
        elif args.line:
            injection_type = 'line'
            if type(args.line) == float:
                line_freq_custom = args.line
            else:
                line_freq_custom = sect_cfg.get('line_freq')
        else:
            injection_type = 'broadband'
            broadband_freqs_custom = arg_dict.get(
                'broadband_freqs', sect_cfg.get('broadband_freqs'))

        auto_broadband_freqs = sect_cfg.get('auto_broadband_freqs')
        auto_comb_freqs = sect_cfg.get('auto_comb_freqs')
        # Ratio plot options
        if args.ratio_plot or args.ratio_plot_only:
            ratio_cfg['plot'] = True

        # Arg parsing plot parameters
        if args.freq_range is not None:
            freq_min, freq_max = args.freq_range
        else:
            freq_min = plot_cfg['freq_min']
            freq_max = plot_cfg['freq_max']
        if args.cf_range is not None:
            factor_min, factor_max = args.cf_range
        else:
            factor_min = plot_cfg['coup_min']
            factor_max = plot_cfg['coup_max']
        if args.amb_range is not None:
            amb_min, amb_max = args.amb_range
        else:
            amb_min = plot_cfg['amb_min']
            amb_max = plot_cfg['amb_max']

        # FFT CALCULATIONS
        logging.info("Calculating FFT parameters.")
        if fft_avg is not None:
            if bin_width is not None:
                logging.info("Bin width and averages given; determining duration.")
                fftlength = int(1. / bin_width)
                duration = fftlength * (1 + ((1 - overlap_pct) * (fft_avg - 1)))
            elif duration is not None:
                logging.info("Duration and averages given; determining bin width.")
                fftlength = round(duration / (1 + ((1 - overlap_pct) * (fft_avg - 1))))
                bin_width = 1. / fftlength
        else:
            if bin_width is not None and duration is not None:
                logging.info("Bin width and duration given; adjusting duration to "
                             "fit an integer number of FFT lengths.")
                fftlength = int(1. / bin_width)
                fft_avg = int(1 + ((duration / fftlength) - 1) / (1 - overlap_pct))
                duration = fftlength * (1 + ((1 - overlap_pct) * (fft_avg - 1)))
            else:
                raise ValueError("insufficient FFT parameters given")
        if verbose:
            print("Duration = {} s".format(duration))
            print("N averages = {}".format(fft_avg))
            print("Bin width = {} s".format(bin_width))
        overlap = fftlength * overlap_pct

        # CHANNEL NAMES AND CALIBRATION DATA
        station = args.station
        channels = sect_cfg['channels']
        if len(args.channel) > 0:
            channels_from_args = []
            channel_names = args.channel
            for channel_name in channel_names:
                if ',' in channel_name:
                    s = channel_name.split(',')
                    channel = PEMChannel(s[0], calibration_factor=s[1])
                elif channel_name in [c.name for c in channels]:
                    channel = next(c for c in channels if c.name == channel_name)
                else:
                    channel = PEMChannel(channel_name)
                channels_from_args.append(channel)
            channels = channels_from_args
        elif args.channel_list:
            logging.info("reading in channel list " + args.channel_list)
            channels_from_cl, factors, units = pem_utils.get_channels(
                args.channel_list,
                station=args.station,
                return_calibration_factors=True)
            channels = [
                PEMChannel(
                    c,
                    calibration_factor=factors.get(c),
                    calibration_unit=units.get(c))
                for c in channels_from_cl]
        if station is not None:
            channels = [c for c in channels if c.station == station]
        channels = [
            c for c in channels if
            pem_utils.search(args.search_channels, c.name)]
        if len(channels) == 0:
            err_msg = "No channels to process."
            logging.error(err_msg)
            raise RuntimeError(err_msg)
        print("\n{} channels(s) found:".format(len(channels)))
        for c in channels:
            print(c)

        # Choose DARM calibration file based on IFO
        gw_channel = PEMChannel(darm_cfg['gw_channel'])
        ifo = gw_channel.ifo
        darm_notch_data = darm_cfg.get('notches', [])
        darm_calibration_file = darm_cfg.get('calibration')

        # CALIBRATION FACTORS
        darm_cal_freqs, darm_cal_factors = preprocess.get_DARM_calibration(
            darm_calibration_file)

        ###########################################################################

        # ===============================
        # LOOP OVER INJECTION NAMES/TIMES
        # ===============================

        logging.info("Beginning to loop over injections.")
        for row in injection_table:
            injection_name, t_bkg, t_inj = row
            if len(injection_table) > 1:
                print("\n" + "*" * 20)
            print("\nAnalyzing injection: " + injection_name)\
            # Get search frequencies from names
            if broadband_freqs_custom is not None:
                broadband_freqs = broadband_freqs_custom
            elif injection_type == 'broadband' and auto_broadband_freqs:
                broadband_freqs = pem_utils.parse_injection_name(
                    injection_name, 'broadband', verbose=verbose)
            else:
                broadband_freqs = None
            if comb_freq_custom is not None:
                comb_freq = comb_freq_custom
            elif injection_type == 'comb' and auto_comb_freqs:
                comb_freq = pem_utils.parse_injection_name(
                    injection_name, 'comb', verbose=verbose)
            else:
                comb_freq = None
            line_freq = line_freq_custom
            # Create injection object
            injection = Injection(
                channels,
                t_bkg,
                t_inj,
                duration,
                name=injection_name,
                ifo=ifo,
                injection_type=injection_type,
                fftlength=fftlength,
                overlap=overlap,
                darm_threshold=darm_threshold,
                sensor_threshold=sensor_threshold,
                sensor_threshold_autoscale=sensor_threshold_autoscale,
                slope_threshold=slope_threshold,
                notch_windows=darm_notch_data,
                peak_search_window=peak_search_window,
                broadband_freqs=broadband_freqs,
                comb_freq=comb_freq,
                line_freq=line_freq)

            # Create subdirectory for this injection
            if injection.name != '':
                sub_dir_basename = injection.name
            else:
                sub_dir_basename = '{}_injection_{}_{}'.format(
                    injection.injection_type.lower(),
                    injection.t_bkg,
                    injection.t_inj)
            sub_dir = os.path.join(out_dir, sub_dir_basename)
            if sub_dir not in sub_dirs:
                sub_dirs[injection.name] = sub_dir
            print("Output directory: " + sub_dir)
            if not os.path.exists(sub_dir):
                os.makedirs(sub_dir)
                logging.info("Subdirectory {} created.".format(sub_dir))

            # PREPARE SENSOR DATA
            print("Fetching and pre-processing data...")
            # Fetch time series and convert to ASDs
            try:
                failed, saturated = injection.get_sensors(
                    ignore_saturated=ignore_saturated, verbose=verbose)
            except RuntimeError:
                print(traceback.format_exc())
                logging.warning("Error with fetching sensor ASDs. "
                                "Moving on to the next injection.")
                continue
            if injection.name not in failed_channels.keys():
                failed_channels[injection.name] = failed
            else:
                failed_channels[injection.name] += failed
            if injection.name not in saturated_channels.keys():
                saturated_channels[injection.name] = saturated
            else:
                saturated_channels[injection.name] += saturated
            # Calibrate ASDs
            injection.calibrate_sensors(verbose=verbose)
            N_uncal = len(injection.uncalibrated_channels)
            if N_uncal > 0:
                print("{} channels were not calibrated:".format(N_uncal))
                for c in injection.uncalibrated_channels:
                    print(c)
                    logging.info("Channel not calibrated: " + c)
            # Generate quadrature-summed sensors for tri-axial sensors.
            if sect_cfg['quad_sum']:
                injection.add_quad_sum_ASDs(verbose=verbose)

            # RATIO PLOT
            if ratio_cfg.get('plot'):
                print("Generating ratio table...")
                ratio_method = ratio_cfg.get('method', 'raw')
                ratio_filename = os.path.join(
                    sub_dir, '{}_{}_ASD_ratios.png'.format(
                        injection.name, section))
                ratio_plots(
                    injection.sensor_bkg_ASDs,
                    injection.sensor_inj_ASDs,
                    ratio_min=ratio_cfg['ratio_min'],
                    ratio_max=ratio_cfg['ratio_max'],
                    method=ratio_method,
                    freq_min=ratio_cfg['freq_min'],
                    freq_max=ratio_cfg['freq_max'],
                    channels_per_plot=ratio_cfg['channels_per_plot'],
                    injection=injection.name,
                    filename=ratio_filename,
                    verbose=verbose)

                # QUIT CALCULATIONS IF RATIO PLOT IS ALL WE WANT
                # (-R option instead of -r)
                if args.ratio_plot_only:
                    continue
                # Using this program like this serves as a quick check
                # of how extensive the effect of a set of injections are, without
                # having to open up dozens of PEM spectra.

            # PREPARE DARM DATA
            injection.get_gw_channel(gw_channel=gw_channel, gate=gate_darm, verbose=verbose)
            if len(darm_cal_factors) > 0:
                injection.calibrate_gw_channel(darm_cal_freqs, darm_cal_factors)

            # CROP ASDs
            # Match frequency domains of DARM and sensor spectra
            # Domain set either by config options (data_freq_min/max)
            # or by smallest domain shared by all spectra
            if verbose:
                print("Cropping ASDs to match frequencies.")
            injection.crop_ASDs(
                fmin=sect_cfg['freq_min'],
                fmax=sect_cfg['freq_max'],
                verbose=verbose)
            if verbose:
                print("ASDs are ready for coupling function calculations.")

            # SMOOTH PARAMETERS
            smooth_dict = pem_utils.get_smooth_params_from_cfg(
                smooth_cfg, injection.channel_names)

            # MEASURE COUPLING FUNCTIONS
            print("Calculating coupling functions...")
            coup_func_list = injection.get_coupling_functions(
                smooth_dict=smooth_dict,
                verbose=verbose)
            for cf in coup_func_list:
                if cf.name not in composite_channels:
                    composite_channels.append(cf.name)
            if injection.name not in composite_injections:
                composite_injections.append(injection.name)

            # SAVE RESULTS
            print("Saving results...")
            if injection_type in ['comb', 'sine']:
                markersizes = tuple([x * 2 for x in plot_cfg['markersizes']])
            else:
                markersizes = plot_cfg['markersizes']
            if injection.name not in channel_info.keys():
                channel_info[injection.name] = {}
            for cf in coup_func_list:
                if verbose:
                    print("Exporting data for {}".format(cf.name))
                base_filename = cf.name.replace('_DQ', '')
                csv_filename = os.path.join(
                    sub_dir, base_filename + '_coupling_data.txt')
                cf_plot_filename = os.path.join(
                    sub_dir, base_filename + '_coupling_plot.png')
                cf_counts_plot_filename = os.path.join(
                    sub_dir, base_filename + '_coupling_counts_plot.png')
                spec_plot_filename = os.path.join(
                    sub_dir, base_filename + '_spectrum.png')
                if np.sum((cf.flags == "Measured") | (cf.flags == "Upper Limit")) == 0:
                    note = "no coupling factors found"
                    channel_info[injection.name][base_filename] = [
                        "",
                        "",
                        "",
                        "./" + os.path.basename(spec_plot_filename),
                        "no coupling factors found"
                    ]
                elif cf.unit == "ct" or cf.unit == "":
                    channel_info[injection.name][base_filename] = [
                        "./" + os.path.basename(csv_filename),
                        "",
                        "./" + os.path.basename(cf_counts_plot_filename),
                        "./" + os.path.basename(spec_plot_filename),
                        "no calibration given"
                    ]
                else:
                    channel_info[injection.name][base_filename] = [
                        "./" + os.path.basename(csv_filename),
                        "./" + os.path.basename(cf_plot_filename),
                        "./" + os.path.basename(cf_counts_plot_filename),
                        "./" + os.path.basename(spec_plot_filename),
                        ""
                    ]
                # Coupling function plot in physical sensor units
                pem_plot.plot_cf(
                    cf,
                    cf_plot_filename,
                    in_counts=False,
                    timestamp=timestamp,
                    freq_min=freq_min,
                    freq_max=freq_max,
                    factor_min=factor_min,
                    factor_max=factor_max,
                    fig_w=plot_cfg['coup_fig_width'],
                    fig_h=plot_cfg['coup_fig_height'],
                    markers=plot_cfg['markers'],
                    markersizes=markersizes,
                    edgewidths=plot_cfg['edgewidths'],
                    facecolors=plot_cfg['facecolors'],
                    edgecolors=plot_cfg['edgecolors']
                )
                # Coupling function in raw sensor counts
                pem_plot.plot_cf(
                    cf,
                    cf_counts_plot_filename,
                    in_counts=True,
                    timestamp=timestamp,
                    freq_min=freq_min,
                    freq_max=freq_max,
                    factor_min=factor_min,
                    factor_max=factor_max,
                    fig_w=plot_cfg['coup_fig_width'],
                    fig_h=plot_cfg['coup_fig_height'],
                    markers=plot_cfg['markers'],
                    markersizes=markersizes,
                    edgewidths=plot_cfg['edgewidths'],
                    facecolors=plot_cfg['facecolors'],
                    edgecolors=plot_cfg['edgecolors']
                )
                # ASD plots showing background/injection and estimated ambient
                pem_plot.plot_injection(
                    cf,
                    spec_plot_filename,
                    timestamp=timestamp,
                    show_darm_threshold=plot_cfg['darm/10'],
                    freq_min=freq_min,
                    freq_max=freq_max,
                    spec_min=amb_min,
                    spec_max=amb_max,
                    fig_w=plot_cfg['spec_fig_width'],
                    fig_h=plot_cfg['spec_fig_height'],
                    markers=plot_cfg['markers'],
                    markersizes=markersizes,
                    edgewidths=plot_cfg['edgewidths'],
                    facecolors=plot_cfg['facecolors'],
                    edgecolors=plot_cfg['edgecolors']
                )
                # CSV data table if coupling function is nonzero
                if any((cf.flags == 'Measured') | (cf.flags == 'Upper Limit')):
                    cf.to_csv(csv_filename)
            logging.info("Coupling functions saved for injection {}."
                         .format(injection.name))
            if verbose and len(injection_table) > 1:
                print("{} complete.".format(injection.name))

        # END OF MAIN LOOP

        print("*" * 20 + "\n")
        timestamp_end = time.time()
        if not args.ratio_plot_only:
            print("All coupling functions finished. (Runtime: {:.3f} s.)"
                  .format(timestamp_end - timestamp))

    # END SECTIONS LOOP

    # Create HTML report
    for injection, sub_dir in sub_dirs.items():
        src = args.config
        dest = os.path.join(sub_dir, 'config.ini')
        shutil.copyfile(src, dest)
        html.write_report(
            sub_dir,
            config=os.path.basename(dest),
            channel_info=channel_info[injection],
            failed_channels=failed_channels[injection],
            saturated_channels=saturated_channels[injection])

    # STOP HERE IF NOT PERFORMING COMPOSITE COUPLING OPTION
    if ((not args.composite)
            or (args.ratio_plot_only)
            or (len(composite_channels) == 0)):
        if len(composite_channels) == 0:
            logging.warning("No coupling functions found.")
        print("Program is finished. (Runtime: {:.3f} s.)"
              .format(timestamp_end - timestamp))
        sys.exit()

    ##########################################################################

    # ============================
    # COMPOSITE COUPLING FUNCTIONS
    # ============================

    print("Generating composite coupling functions.")
    comp_out_dir = os.path.join(out_dir, comp_cfg.get('composite_dir', ''))
    composite_channels.sort()
    composite_injections.sort()
    custom_options = [
        "--directory", out_dir,
        "--output", comp_out_dir,
        "--search-channels", "|".join(composite_channels),
        "--search-injections", "|".join(composite_injections)
    ]
    custom_args = [args.config] + args.section + custom_options
    print("pemcoupling-composite " + " ".join(custom_args))
    if args.summary:
        custom_args.append("--summary")
    args = parse.parse_args('pemcoupling-composite', args=custom_args)
    composite_main(args)
    timestamp_comp = time.time()
    print("Program is finished. (Runtime: {:.3f} s.)"
          .format(timestamp_comp - timestamp))
