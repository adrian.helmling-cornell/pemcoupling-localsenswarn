import logging
import time
import numpy as np

from . import utils as pem_utils
from . import preprocess
from .pemchannel import PEMChannel
from .coupfunc import CoupFunc


class Injection(object):
    """
    Object for handling injection parameters and spectra,
    and generating coupling functions.
    """

    def __init__(
            self,
            channels,
            t_bkg,
            t_inj,
            duration,
            name=None,
            ifo=None,
            injection_type=None,
            fftlength=None,
            overlap=None,
            gw_channel=None,
            smooth_dict={},
            calibration_factors={},
            calibration_units={},
            darm_threshold=2,
            sensor_threshold=2,
            slope_threshold=None,
            notch_windows=[],
            peak_search_window=0,
            broadband_freqs=None,
            comb_freq=None,
            line_freq=None,
            auto_freq_bands=False,
            sensor_threshold_autoscale=None
        ):
        self._init_channels(
            channels,
            calibration_factors,
            calibration_units)
        self.t_bkg = t_bkg
        self.t_inj = t_inj
        self.duration = duration
        self.name = name
        self.ifo = ifo
        self.injection_type = injection_type
        self._init_gw_channel(gw_channel)

        # Parameters for fetching and pre-processing spectra
        self.fftlength = fftlength
        self.overlap = overlap
        self.smooth_dict = smooth_dict

        # Parameters for computing coupling functions
        self.darm_threshold = darm_threshold
        self.sensor_threshold = sensor_threshold
        self.sensor_threshold_autoscale = sensor_threshold_autoscale
        self.slope_threshold = slope_threshold
        self.notch_windows = notch_windows
        self.peak_search_window = peak_search_window
        self.broadband_freqs = broadband_freqs
        self.comb_freq = comb_freq
        self.line_freq = line_freq
        self.auto_freq_bands = auto_freq_bands

    def _init_channels(
            self, channels, calibration_factors, calibration_units):
        _channels = []
        for channel in channels:
            if type(channel) == str:
                _channels.append(PEMChannel(
                    channel,
                    calibration_factors.get(channel),
                    calibration_units.get(channel)))
            elif type(channel) == PEMChannel:
                name = channel.name
                if name in calibration_factors.keys():
                    channel.calibration_factor = calibration_factors[name]
                if name in calibration_units.keys():
                    channel.calibration_unit = calibration_units[name]
                _channels.append(channel)
        self._channels = _channels

    def _init_gw_channel(self, channel):
        if type(channel) == str:
            self.gw_channel = PEMChannel(channel)
        elif type(channel) == PEMChannel:
            self.gw_channel = channel
        elif channel is None:
            self.gw_channel = None

    @property
    def channels(self):
        if hasattr(self, 'sensor_bkg_ASDs'):
            return [asd.channel for asd in self.sensor_bkg_ASDs]
        else:
            return self._channels

    @property
    def channel_names(self):
        return [channel.name for channel in self.channels]

    @property
    def n_channels(self):
        return len(self.channels)

    @property
    def uncalibrated_channels(self):
        return [
            asd.name for asd in self.sensor_bkg_ASDs if (
                asd.unit is None or
                asd.unit == 'Counts')
        ]

    def set_fundamental_freq(self, verbose=False):
        """Set fundamental frequency based on injection name."""
        self.freq_search = pem_utils.freq_search(self.name, verbose=verbose)

    def get_sensors(self, ignore_saturated=True, verbose=False):
        """
        Extract time series data for a list of channel(s).

        Parameters
        ----------
        ignore_saturated : {True, False}
            Skip channels whose raw time series exceed 32000 counts.
        verbose : {False, True}

        Returns
        -------
        failed_channels : list of `pemchannel.PEMChannel` objects
            Channels that failed to fetch from nds2.
        """
        timestamp_1 = time.time()
        ts_inj_list = []
        ts_bkg_list = []
        failed_channels = []
        # Fetch background and injection time series for each channel
        for channel in self.channels:
            try:
                start = self.t_inj
                end = self.t_inj + self.duration
                ts_inj = channel.ts(start, end, allow_tape=1)
                start = self.t_bkg
                end = self.t_bkg + self.duration
                ts_bkg = channel.ts(start, end, allow_tape=1)
            except RuntimeError:
                failed_channels.append(channel.name)
                logging.warning(
                    "Channel failed to load: {} at time {} to {}."
                    .format(channel, start, end))
            else:
                ts_inj_list.append(ts_inj)
                ts_bkg_list.append(ts_bkg)
                logging.info(
                    "Channel successfully extracted: {} at time {} to {}"
                    .format(channel, start, end))
        # Warning if no time series found
        if len(ts_inj_list) == 0:
            logging.warning(
                "No channels were successfully extracted. "
                "Check that channel names and times are valid.")
        # Show any channels that failed
        elif len(failed_channels) > 0:
            print("Failed to extract {} of {} channels:"
                  .format(len(failed_channels), len(self.channels)))
            for c in failed_channels:
                print(c)
            print("Check that these channel names and times are valid.")
        timestamp_2 = time.time()
        if verbose:
            print("All channel time series extracted. (Runtime: {:.1f} s)"
                  .format(timestamp_2 - timestamp_1))
        # Raise error if no channels were successfully imported
        if len(failed_channels) == len(self.channels):
            raise RuntimeError("All channels failed to load.")
        # Remove saturated channels
        if ignore_saturated:
            saturated_channels = [
                ts.channel.name for ts in ts_inj_list if ts.saturated()]
        else:
            saturated_channels = []
        unsaturated_channels = [
            ts.channel.name for ts in ts_inj_list if
            ts.channel.name not in saturated_channels]
        # Raise error if all channels were saturated
        if len(unsaturated_channels) == 0:
            raise RuntimeError(
                "All channels were rejected due to saturation.")
        ts_inj_list = [
            ts for ts in ts_inj_list if ts.channel.name in unsaturated_channels]
        ts_bkg_list = [
            ts for ts in ts_bkg_list if ts.channel.name in unsaturated_channels]
        self.sensor_bkg_ASDs = [
            ts.asd(self.fftlength, self.overlap) for ts in ts_bkg_list]
        self.sensor_inj_ASDs = [
            ts.asd(self.fftlength, self.overlap) for ts in ts_inj_list]
        timestamp_3 = time.time()
        if verbose:
            print("Sensor ASDs acquired. (Runtime: {:1f} s)"
                  .format(timestamp_3 - timestamp_2))
        return failed_channels, saturated_channels

    def calibrate_sensors(
            self, calibration_factors={}, calibration_units={},
            verbose=False):
        for asd in self.sensor_bkg_ASDs + self.sensor_inj_ASDs:
            try:
                calibration_factor = calibration_factor[asd.channel.name]
            except:
                calibration_factor = None
            try:
                calibration_unit = calibration_units[asd.channel.name]
            except KeyError:
                calibration_unit = None
            asd.calibrate(calibration_factor, calibration_unit)

    def add_quad_sum_ASDs(self, verbose=False):
        if verbose:
            print('Generating quadrature sum channel from '
                  'single-component channels.')
        # Compute quadrature-sum spectra
        qsum_sensor_bkg_ASDs = preprocess.quad_sum_ASD(self.sensor_bkg_ASDs)
        qsum_sensor_inj_ASDs = preprocess.quad_sum_ASD(self.sensor_inj_ASDs)
        # Combine quad-sum spectra with original channels and
        # sort the resulting list
        combined_sensor_bkg_ASDs = self.sensor_bkg_ASDs + qsum_sensor_bkg_ASDs
        combined_sensor_inj_ASDs = self.sensor_inj_ASDs + qsum_sensor_inj_ASDs
        names = [asd.name for asd in combined_sensor_inj_ASDs]
        names_sorted = sorted(names)
        sensor_bkg_ASDs, sensor_inj_ASDs = [], []
        while len(names_sorted) > 0:
            m = names_sorted.pop(0)
            sensor_bkg_ASDs.append(combined_sensor_bkg_ASDs[names.index(m)])
            sensor_inj_ASDs.append(combined_sensor_inj_ASDs[names.index(m)])
        self.sensor_bkg_ASDs = sensor_bkg_ASDs
        self.sensor_inj_ASDs = sensor_inj_ASDs

    def get_gw_channel(self, gw_channel=None, gate=False, verbose=False):
        timestamp_1 = time.time()
        if gw_channel is None:
            gw_channel = self.gw_channel
            if gw_channel is None:
                raise ValueError("No GW channel given.")
        # Fetch and calibrated DARM ASDs in one function:
        if verbose:
            print("Fetching {}...".format(gw_channel))
        try:
            start = self.t_inj
            end = self.t_inj + self.duration
            ts_inj = gw_channel.ts(start, end, gate=gate, allow_tape=1)
            start = self.t_bkg
            end = self.t_bkg + self.duration
            ts_bkg = gw_channel.ts(start, end, gate=gate, allow_tape=1)
        except RuntimeError:
            failed_channels.append(channel.name)
            logging.warning(
                "Channel failed to load: {} at time {} to {}."
                .format(channel, start, end))
        if verbose:
            print("Computing {} ASDs...".format(gw_channel))
        gw_bkg = ts_bkg.asd(fftlength=self.fftlength, overlap=self.overlap)
        gw_inj = ts_inj.asd(fftlength=self.fftlength, overlap=self.overlap)
        # Convert strain channel to meters
        if 'STRAIN' in gw_channel.name:
            gw_bkg.calibrate(4000.0, 'm')
            gw_inj.calibrate(4000.0, 'm')
        # Pair each sensor ASD with a clone of DARM bkg/inj ASDs
        self.gw_bkg_ASDs = [
            gw_bkg.copy() for _ in range(self.n_channels)]
        self.gw_inj_ASDs = [
            gw_inj.copy() for _ in range(self.n_channels)]
        timestamp_2 = time.time()
        if verbose:
            print('{} ASDs acquired. (Runtime: {:4f} s.'
                  .format(gw_channel, timestamp_2 - timestamp_1))

    def calibrate_gw_channel(self, freqs, factors):
        for asd in self.gw_bkg_ASDs:
            factors_interp = np.interp(asd.freqs, freqs, factors)
            asd.calibrate(factors_interp, 'm')
        for asd in self.gw_inj_ASDs:
            factors_interp = np.interp(asd.freqs, freqs, factors)
            asd.calibrate(factors_interp, 'm')

    def crop_ASDs(self, fmin=None, fmax=None, verbose=False):
        for i in range(self.n_channels):
            sens_freqs = self.sensor_bkg_ASDs[i].freqs
            darm_freqs = self.gw_bkg_ASDs[i].freqs
            fmin_ = max([fmin, sens_freqs[0], darm_freqs[0]])
            if fmax is None:
                fmax_ = min([sens_freqs[-1], darm_freqs[-1]])
            else:
                fmax_ = min([fmax, sens_freqs[-1], darm_freqs[-1]])
            self.sensor_bkg_ASDs[i].crop(fmin_, fmax_)
            self.sensor_inj_ASDs[i].crop(fmin_, fmax_)
            self.gw_bkg_ASDs[i].crop(fmin_, fmax_)
            self.gw_inj_ASDs[i].crop(fmin_, fmax_)

    def get_max_ratios(self, sensor_type, quad_sum=False):
        quad_sum_tags = ['XYZ', 'PITYAW']
        if quad_sum:
            channels = [
                (i, c) for i, c in enumerate(self.channels) if
                c.sensor == sensor_type and c.axis in quad_sum_tags
            ]
        else:
            channels = [
                (i, c) for i, c in enumerate(self.channels) if
                c.sensor == sensor_type and c.axis not in quad_sum_tags
            ]
        if len(channels) > 0:
            ratios_list = []
            for i, channel in channels:
                freqs = self.sensor_inj_ASDs[i].freqs
                sens_bg = self.sensor_bkg_ASDs[i].values
                sens_inj = self.sensor_inj_ASDs[i].values
                ratios = pem_utils.nonzero_divide(sens_inj, sens_bg)
                ratios_list.append(ratios)
            len_ratios = max(len(ratios) for ratios in ratios_list)
            for i, ratios in enumerate(ratios_list):
                num = len_ratios - len(ratios)
                if num > 0:
                    ratios = np.append(ratios, np.ones(num))
                ratios_list[i] = ratios
            ratios = np.column_stack(ratios_list)
            max_ratios = ratios.max(axis=1)
        else:
            max_ratios = None
        return max_ratios

    def get_max_ratios_dict(self):
        sensor_types = [c.sensor for c in self.channels]
        sensor_types = list(set(sensor_types))
        ratios_dict = {}
        for sensor_type in sensor_types:
            ratios_dict[sensor_type + '_quad_sum'] = self.get_max_ratios(
                sensor_type, quad_sum=True)
            ratios_dict[sensor_type] = self.get_max_ratios(
                sensor_type, quad_sum=False)
        return ratios_dict

    def get_autoscaled_thresholds(self, scale, threshold):
        ratios_dict = self.get_max_ratios_dict()
        thresh_dict = {}
        for k, v in ratios_dict.items():
            if v is not None:
                thresh = np.maximum(threshold, scale * v)
            else:
                thresh = None
            thresh_dict[k] = thresh
        return thresh_dict

    def get_coupling_functions(self, verbose=False, **kwargs):
        timestamp_1 = time.time()
        cf_keywords = [
            'darm_threshold',
            'sensor_threshold',
            'sensor_threshold_autoscale',
            'slope_threshold',
            'injection_type',
            'smooth_dict',
            'notch_windows',
            'peak_search_window',
            'broadband_freqs',
            'comb_freq',
            'line_freq']
        for key in cf_keywords:
            if key not in kwargs.keys():
                kwargs[key] = getattr(self, key)
        smooth_dict = kwargs.pop('smooth_dict')
        sensor_threshold_autoscale = kwargs.pop(
            'sensor_threshold_autoscale')
        if sensor_threshold_autoscale is not None:
            min_sensor_threshold = kwargs.get('sensor_threshold', 1)
            scaled_sensor_threshold_dict = self.get_autoscaled_thresholds(
                sensor_threshold_autoscale, min_sensor_threshold)
        out = []
        for i, channel in enumerate(self.channels):
            smooth_params = (0, 0)
            smooth_log = False
            if channel.name in smooth_dict.keys():
                x = smooth_dict[channel.name]
                try:
                    smooth_params = (x[0], x[1])
                    smooth_log = (x[2])
                except (TypeError, IndexError):
                    pass
            if sensor_threshold_autoscale:
                if channel.axis in ['XYZ', 'PITYAW']:
                    key = channel.sensor + '_quad_sum'
                else:
                    key = channel.sensor
                scaled_sensor_threshold = scaled_sensor_threshold_dict[key]
                kwargs.update(scaled_sensor_threshold=scaled_sensor_threshold)
            cf = CoupFunc.compute(
                self.sensor_bkg_ASDs[i],
                self.sensor_inj_ASDs[i],
                self.gw_bkg_ASDs[i],
                self.gw_inj_ASDs[i],
                smooth_params=smooth_params,
                smooth_log=smooth_log,
                injection_name=self.name,
                verbose=verbose,
                **kwargs)
            out.append(cf)
            logging.info('Coupling function complete: {}.'
                         .format(channel))
        timestamp_2 = time.time()
        if verbose:
            print('Coupling functions calculated. (Runtime: {:4f} s)'
                  .format(timestamp_2 - timestamp_1))
        return out
