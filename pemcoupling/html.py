import os
import glob
from . import htmllib


BGCOLORS = {
    "success": "PaleGreen",
    "uncalibrated": "Khaki",
    "no coupling": "Orange",
    "saturated": "Tomato",
    "failed": "Tomato"
}


def write_report(sub_dir, config=None, channel_info=[], failed_channels=[], saturated_channels=[]):
    """
    Generate HTMl report for a PEM injection with links to ``pemcoupling`` output.

    Parameters
    ----------
    sub_dir : str
        Subdirectory containing ``pemcoupling`` results
    channel_links : dict of dicts
        Links to data and plots for each channel
    failed_channels : list
        Channels GWPy failed to load
    saturated_channels : list
        Channels that were ignored due to saturation
    """

    html_filename = os.path.join(sub_dir, 'index.html')
    title = "PEMCoupling results ({})".format(os.path.basename(sub_dir))
    html = (
        "<title>{title}</title>\n"
        "<style>\n"
        "* {{font-family: Calibri}}\n"
        "</style>\n"
        "<h1>{title}</h1>\n"
    ).format(title=title)
    # Add config file link
    if config is not None:
        config_filename = os.path.join('.', os.path.basename(config))
        html += "<a href=\"{}\">Configuration file</a>\n".format(config)
    # Find ratio plot URLs
    glob_pattern = os.path.join(sub_dir, "*ratios*.png")
    ratio_plot_links = sorted([
        os.path.basename(f) for f in glob.glob(glob_pattern)])
    ratio_plots_str = "\n".join([
        "<p>{}</p>".format(htmllib.link(link, "./" + link))
        for link in ratio_plot_links])
    # Add ratio plot URLS to HTML string
    if len(ratio_plots_str) > 0:
        html += "<h2>Ratio plots</h2>\n"
        html += ratio_plots_str
    # Create list of table row strings
    rows = []
    # Add coupling functions with links
    channels = list(channel_info.keys()) + failed_channels + saturated_channels
    channels = sorted(set(channels))
    for channel in channels:
        status = "success"
        note = ""
        csv = ""
        cf_plot = ""
        cf_counts_plot = ""
        spec_plot = ""
        if channel in failed_channels:
            status = "failed"
            note = "failed to load from NDS2"
        elif channel in saturated_channels:
            status = "saturated"
            note = "channel saturated during injection"
        elif channel in channel_info.keys():
            info = channel_info[channel]
            csv, cf_plot, cf_counts_plot, spec_plot, note = info
            if note == "":
                status = "success"
            elif cf_plot == "" and cf_counts_plot != "":
                status = "uncalibrated"
            else:
                status = "no coupling"
        cells = [channel, "", "", "", note]
        if csv != "":
            cells[1] += htmllib.link("ASCII data", csv)
        if cf_plot != "":
            cells[2] += htmllib.link(
                "coupling function (calibrated)", cf_plot) + "<br>"
        if cf_counts_plot != "":
            cells[2] += htmllib.link(
                "coupling function (uncalibrated)",
                cf_counts_plot) + "<br>"
        if spec_plot != "":
            if status == "no coupling":
                cells[3] += htmllib.link(
                    "spectra", spec_plot)
            else:
                cells[3] += htmllib.link(
                    "spectra and estimated ambient", spec_plot)
        bgcolor = BGCOLORS[status]
        row = htmllib.TableRow(cells=cells, bgcolor=bgcolor)
        rows.append(row)
    header_row = [
        "Channel",
        "Data",
        "Coupling function plots",
        "Amplitude spectra and estimated ambient plots",
        "Notes"
    ]
    table = htmllib.table(rows, header_row=header_row, border=1)
    # Add table to HTML string
    html += "<h2>Coupling function results</h2>\n"
    html += (
        "Key:<br>"
        "Green = success<br>"
        "Yellow = no calibration data given<br>"
        "Orange = no coupling factors found<br>"
        "Red = could not fetch channel, or channel was rejected due to saturation<br>")
    html += str(table)
    # Write to file
    with open(html_filename, 'w') as f:
        f.write(html)
    return


def write_composite_report(out_dir, channels, config=None, colors={}):
    """
    Generate HTMl report for ``pemcoupling-composite`` results.

    Parameters
    ----------
    out_dir : str
        Subdirectory containing ``pemcoupling-composite`` results
    channels : list
        Channel names
    config : str
        Link to config file
    colors : dict
        Hex color for each channel/row
    """

    html_filename = os.path.join(out_dir, 'index.html')
    title = "Composite coupling function results"
    html = (
        "<title>{title}</title>\n"
        "<style>\n"
        "* {{font-family: Calibri}}\n"
        "</style>\n"
        "<h1>{title}</h1>\n"
    ).format(title=title)
    # Add config file link
    if config is not None:
        config_filename = os.path.join('.', os.path.basename(config))
        html += "<a href=\"{}\">Configuration file</a><br><br>\n".format(config)
    rows = []
    for channel in sorted(channels):
        base_filename = os.path.join('./{}_composite_'.format(channel))
        cells = [
            channel,
            htmllib.link(
                "ASCII data", base_filename + 'coupling_data.txt'),
            "<br>".join([
                htmllib.link(
                    "coupling function (calibrated, injections labeled)",
                    base_filename + 'coupling_multi_plot.png'),
                htmllib.link(
                    "coupling function (calibrated)",
                    base_filename + 'coupling_plot.png'),
                htmllib.link(
                    "coupling function (uncalibrated)",
                    base_filename + 'coupling_counts_plot.png')
            ]),
            "<br>".join([
                htmllib.link(
                    "estimated ambient (injections labeled)",
                    base_filename + 'est_amb_multi_plot.png'),
                htmllib.link(
                    "estimated ambient",
                    base_filename + 'est_amb_plot.png')
            ])
        ]
        bgcolor = colors.get(channel)
        row = htmllib.TableRow(cells=cells, bgcolor=bgcolor)
        rows.append(row)
    header_row = [
        "Channel",
        "Data",
        "Composite coupling function plots",
        "Composite estimated ambient plots"
    ]
    table = htmllib.table(rows, header_row=header_row, border=1)
    # Add table to HTML string
    html += (
        "Table rows are colored red if the estimated ambient from "
        "measurements exceeds DARM / 10 (the redder the channel, the "
        "closer it is to the DARM background).<br>White rows are at "
        "least a factor of 10 below DARM.<br>")
    html += str(table)
    # Write to file
    with open(html_filename, 'w') as f:
        f.write(html)
    return
