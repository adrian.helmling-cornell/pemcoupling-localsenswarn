import os
import glob
from gwpy.time import to_gps


def get_input(arg):
    """
    Handle command-line inputs for interactive mode
    """
    try:
        func = raw_input   # python 2
    except NameError:
        func = input       # python 3
    return func(arg)


def check_input(prompt, func=None, cond=None, err=None):
    """
    Display a prompt and handle user input
    """
    while True:
        inp = get_input(prompt).strip()
        if func:
            inp2 = func(inp)
        else:
            inp2 = inp
        if cond:
            status = cond(inp2)
        else:
            status = True
        if status:
            return inp2
        elif isinstance(err, str):
            print(err)
        elif callable(err):
            print(err(inp2))
        else:
            pass


def menu_selection(x):
    try:
        return int(x)
    except:
        return None


def interactive_session(script, parser):
    args = interactive_config_args()
    args += interactive_injection_args()
    args += interactive_channel_args()
    print(" ".join([script] + args))
    return parser.parse_args(args)


def interactive_config_args():
    def check_sections(sections):
        out = True
        for section in sections:
            with open(config_file, 'r') as f:
                if '[{}]'.format(section) not in f.read():
                    out = False
        return out
    config_file = check_input(
        "provide a config file: ",
        cond=os.path.exists,
        err=lambda x: "configuration file {} not found".format(x)
    )
    sections = check_input(
        "name one or more sections from the config file to use: ",
        func=lambda x: x.split(),
        cond=check_sections,
        err="config file is missing one or more sections provided"
    )
    return [config_file] + sections


def interactive_injection_args():
    print(
        "methods for providing injection times:\n" +
        "    0: single background time and injection time\n" +
        "    1: table of injections (each row contains <name>,<bkg time>,<inj time>)\n" +
        "    2: one or more DTT files"
    )
    injection_method = check_input(
        "choose a method: ",
        func=menu_selection,
        cond=lambda x: x in (0, 1, 2),
        err="invalid input"
    )
    args = []
    if injection_method == 0:
        def check_time(t):
            try:
                to_gps(t)
            except:
                return False
            else:
                return True
        t_bkg = check_input("provide background time (UTC or GPS): ", cond=check_time, err="invalid time")
        t_inj = check_input("provide injection time (UTC or GPS): ", cond=check_time, err="invalid time")
        args += ['-t', t_bkg, t_inj]
    elif injection_method == 1:
        injection_list = check_input(
            "provide a file containing a table of injections: ",
            cond=os.path.exists,
            err=lambda x: "injection file {} not found".format(x)
        )
        search_injections = get_input("provide a search pattern to narrow down injection names (optional): ").strip()
        if len(search_injections) > 0:
            args += ['--search-injections', search_injections]
        args = ['-I', injection_list]
    elif injection_method == 2:
        dtts = get_input("provide one or more DTT files containing background and injection references: ").strip().split()
        dtts_glob = []
        for dtt in dtts:
            if '*' in dtt:
                dtts_glob += glob.glob(dtt)
            elif not os.path.exists(dtt):
                raise_input_err("DTT file {} not found".format(dtt), OSError)
        dtts = [dtt for dtt in dtts if '*' not in dtt]
        args += ['-d'] + dtts + dtts_glob
    return args


def interactive_channel_args():
    print(
        "methods for selecting channels:\n" +
        "    0: use channels in config file\n" +
        "    1: channel list file\n" +
        "    2: one or more individual channel names"
    )
    channels_method = check_input(
        "choose a method: ",
        func=menu_selection,
        cond=lambda x: x in (0, 1, 2),
        err="invalid input"
    )
    args = []
    if channels_method == 0:
        pass
    elif channels_method == 1:
        channel_list = check_input(
            "provide a channel list file: ",
            cond=os.path.exists,
            err=lambda x: "channel list {} not found".format(x)
        )
        args += ['-C', channel_list]
    elif channels_method == 2:
        channels = check_input(
            "provide channel name(s) [format: CHANNEL_NAME or CHANNEL_NAME,CALIBRATION_FACTOR]: ",
            func=lambda x: x.split()
        )
        for channel in channels:
            args += ['-c', channel]
    else:
        print("Invalid input.")
    station = get_input("choose a station to narrow down channels (optional) (CS/EX/EY): ")
    if station in ('CS', 'EX', 'EY'):
        args += ['--station', station]
    if channels_method in (0, 1):
        search_channels = get_input("provide a search pattern narrow down channel names (optional): ").strip()
        if len(search_channels) > 0:
            args += ['--search-channels', search_channels]
    return args
