import numpy as np
import logging
import warnings

from gwpy.detector import Channel
from gwpy.timeseries import TimeSeries

# Fix usetex parameter changed by gwpy
import matplotlib
matplotlib.rcParams['text.usetex'] = False
matplotlib.rcParams['font.family'] = 'sans-serif'

# Defaults for PEM units, quantity names, and coupling types by
# sensor/subsystem
QTY_DICT = {
    'm'   : 'Displacement',
    'm/s' : 'Velocity',
    'm/s2': 'Acceleration',
    'Pa'  : 'Pressure',
    'T'   : 'Magnetic Field',
    'beam diameters': 'Beam Jitter'
}
COUPLING_DICT = {
    'MIC'  : 'Acoustic',
    'MAG'  : 'Magnetic',
    'RADIO': 'RF',
    'SEIS' : 'Seismic',
    'ISI'  : 'Seismic',
    'ACC'  : 'Vibrational',
    'HPI'  : 'Seismic',
    'ADC'  : 'Vibrational',
    'WFS'  : 'Beam Jitter'
}
# Defaults for units read in from calibration file
CALIB_UNITS_DICT = {
    'ACC': 'm/s2',
    'HPI': 'm/s',
    'ISI': 'm/s',
    'MAG': 'T',
    'MIC': 'Pa',
    'SEI': 'm/s',
    'WFS': 'beam diameters'
}


class PEMChannel(Channel):
    def __init__(self, name, calibration_factor=None, calibration_unit=None):
        super(PEMChannel, self).__init__(name)
        self.calibration_factor = calibration_factor
        self.calibration_unit = calibration_unit
        # Parse sensor name
        self.station = self._subsystem
        if self.system == 'IMC' and self.station == 'WFS':
            self.station = 'CS'
            self.sensor = 'WFS'
            signal = self.signal.split('_')
            attrs = ['location, context, axis']
            for idx, attr in enumerate(attrs):
                try:
                    setattr(self, attr, signal[idx])
                except IndexError:
                    setattr(self, attr, None)
        elif self.signal:
            signal = self.signal.split('_')
            attrs = ['sensor', 'location', 'context', 'axis']
            for idx, attr in enumerate(attrs):
                try:
                    setattr(self, attr, signal[idx])
                except IndexError:
                    setattr(self, attr, None)
        else:
            raise ValueError(
                "Invalid channel name {}".format(self.name))

    @property
    def daq_name(self):
        if 'STRAIN' in self.name:
            return self.name
        elif '_DQ' not in self.name:
            return self.name + '_DQ'
        else:
            return self.name

    @property
    def qty(self):
        if self.unit is not None:
            unit = str(self.unit)
            if unit in QTY_DICT.keys():
                return QTY_DICT[unit]
            else:
                return ''
        else:
            return ''

    @property
    def calibration_qty(self):
        try:
            return QTY_DICT[self.calibration_unit]
        except KeyError:
            return ''

    @property
    def coupling(self):
        if self.sensor in COUPLING_DICT.keys():
            return COUPLING_DICT[self.sensor]
        elif self.system in COUPLING_DICT.keys():
            return COUPLING_DICT[self.subsystem]
        else:
            return ''

    def ts(self, start, end, **kwargs):
        ts = PEMTimeSeries.get(
            start, end, channel=self, **kwargs)
        return ts


class PEMTimeSeries(TimeSeries):
    def __new__(cls, ts, channel):
        new = super(PEMTimeSeries, cls).__new__(
            cls, ts.value, name=ts.name, unit=ts.unit,
            channel=ts.channel, t0=ts.t0.value, dt=ts.dt.value)
        new.channel = channel
        return new

    @classmethod
    def get(cls, start, end, channel=None, gate=False, **kwargs):
        warning_message = (
            "unique NDS2 channel match not found for '{}'"
            .format(channel.daq_name))
        warnings.filterwarnings(
            'error', message=warning_message)
        try:
            ts_ = super(PEMTimeSeries, cls).get(
                channel.daq_name, start, end, **kwargs)
        except Warning as w:
            raise RuntimeError(w)
        if gate:
            ts_ = cls._gate_glitches(ts_)
        ts = cls.__new__(cls, ts_, channel)
        return ts

    def asd(self, fftlength=None, overlap=None, calibration=None):
        asd_ = TimeSeries.asd(self, fftlength, overlap)
        name = asd_.name
        freqs = asd_.frequencies.value
        value = asd_.value
        t0 = asd_.epoch.value
        channel=asd_.channel
        duration = self.xspan[1] - self.xspan[0]
        n_avg = int(1 + (duration - fftlength) / (fftlength - overlap))
        fseries = PEMFrequencySeries(
            name,
            freqs,
            value,
            t0=t0,
            channel=channel,
            calibration=calibration,
            n_avg=n_avg)
        return fseries

    def saturated(self):
        return self.value.max() > 32000

    @classmethod
    def _gate_glitches(self, ts, blrms_bp=(50, 200), blrms_stride=1, thresh=2, t_pad=0.5, alpha=0.5):
        """
        Gate glitches in a timeseries using a Tukey window.
        """

        blrms = ts.bandpass(*blrms_bp).rms(blrms_stride)
        blrms_norm = blrms / blrms.median()
        # Find peaks based on excess BLRMS
        peaks = []
        i = 0
        while i < len(blrms_norm):
            if blrms_norm[i] >= thresh:
                start = i
                i += 1
                while i < len(blrms_norm):
                    end = i
                    if blrms_norm[i] >= thresh:
                        i += 1
                    else:
                        break
                peaks.append((start, end))
            else:
                i += 1
        ts_gated = ts.copy()
        for peak in peaks:
            window_idx = (
                (ts.times.value >= blrms.times.value[peak[0]] - t_pad) &
                (ts.times.value <= blrms.times.value[peak[1]] + t_pad)
            )
            if window_idx.sum() > 0:
                N = len(ts[window_idx])
                # Tukey window
                window = np.ones(N)
                for n in range(N):
                    if n <= alpha * N / 2:
                        window[n] = 0.5 * (1 + np.cos(np.pi * (2 * n / (alpha * N) - 1)))
                    elif n >= (1 - alpha / 2) * N:
                        window[n] = 0.5 * (1 + np.cos(np.pi * (2 * n / (alpha * N) - (2 / alpha) + 1)))
                # Apply gating
                ts_gated[window_idx] *= (1 - window)
        return ts_gated


class PEMFrequencySeries(PEMChannel):
    """
    Calibrated amplitude spectral density of a single channel.

    Attributes
    ----------
    name : str
        Channel name.
    freqs : array
        Frequencies.
    values : array
        Calibrated ASD values.
    t0 : float, int
        Start time of channel.
    unit : str
        Unit of measurement for 'values'
    calibration : array-like
        Calibration factors across all frequencies.
    """

    def __init__(
            self, name, freqs, values, t0=None, channel=None,
            unit=None, calibration=None, n_avg=1):
        """
        Parameters
        ----------
        name : str
        freqs : array
        values : array
        t0 : float, int, optional
        unit : str, optional
        calibration : array-like, optional
        """

        super(PEMFrequencySeries, self).__init__(name)
        freqs = np.asarray(freqs)
        values = np.asarray(values)
        self.freqs = freqs[freqs > 0]
        self.values = values[freqs > 0]
        self.t0 = t0
        self.channel = channel
        self.unit = unit
        if calibration is not None:
            self.calibration = calibration[freqs > 0]
        else:
            self.calibration = None
        self.n_avg = n_avg

    @property
    def df(self):
        try:
            return self.freqs[1] - self.freqs[0]
        except IndexError:
            return None

    def calibrate(self, calibration_factor=None, calibration_unit=None):
        """
        Calibrate channel.

        Parameters
        ----------
        calibration_factor : float, int
            Conversion factor from ADC counts to physical units.
        calibration_unit : str, optional
            Physical unit to assign to calibrated data.
        """

        if calibration_factor is None:
            calibration_factor = self.channel.calibration_factor
        if calibration_unit is None:
            calibration_unit = self.channel.calibration_unit
        if calibration_factor is not None:
            calibration_factor = np.abs(calibration_factor)
            if calibration_unit is None:
                # Use defaults if no units are given
                if self.channel.sensor in CALIB_UNITS_DICT.keys():
                    calibration_unit =  CALIB_UNITS_DICT[self.channel.sensor]
                elif self.channel.system in CALIB_UNITS_DICT.keys():
                    calibration_unit =  CALIB_UNITS_DICT[self.channel.system]
            try:
                iter(calibration_factor)
            except TypeError:
                self.calibration = np.ones_like(self.values) * calibration_factor
            else:
                if len(calibration_factor) == len(self.values):
                    self.calibration = calibration_factor
                else:
                    raise IndexError(
                        "calibration_factor must be of length 1 or same "
                        "length as ASD.")
            if calibration_unit is not None:
                # Convert m/s and m/s2 into displacement
                if calibration_unit == 'm/s':
                    # Divide by omega
                    self.calibration /= self.freqs * (2 * np.pi)
                    calibration_unit = 'm'
                elif calibration_unit == 'm/s2':
                    # Divide by omega^2
                    self.calibration /= (self.freqs * (2 * np.pi))**2
                    calibration_unit = 'm'
            # Apply calibration and assign new (physical units)
            self.values *= self.calibration
            self.unit = calibration_unit
        else:
            print("no calibration factor for " + self.channel.name)

    def _crop_idx(self, fmin, fmax):
        """
        Crop ASD between fmin and fmax.

        Parameters
        ----------
        fmin : float, int
            Minimum frequency (Hz).
        fmax : float, int
            Maximum frequency (Hz).
        """
        if fmin is not None:
            try:
                fmin = float(fmin)
            except ValueError:
                raise ValueError(
                    "Crop requires floats for fmin, fmax.")
        if fmax is not None:
            try:
                fmax = float(fmax)
            except ValueError:
                raise ValueError(
                    "Crop requires floats for fmin, fmax.")
        # Determine start and end indices from fmin and fmax
        if (fmin is not None) and (fmin > self.freqs[0]):
            start = int(float(fmin - self.freqs[0]) / float(self.df))
        else:
            start = None
        if (fmax is not None) and (fmax <= self.freqs[-1]):
            end = int(float(fmax - self.freqs[0]) / float(self.df))
        else:
            end = None
        return start, end

    def crop(self, fmin, fmax):
        """
        Crop ASD between fmin and fmax.

        Parameters
        ----------
        fmin : float, int
            Minimum frequency (Hz).
        fmax : float, int
            Maximum frequency (Hz).
        """
        start, end = self._crop_idx(fmin, fmax)
        self.freqs = self.freqs[start:end]
        self.values = self.values[start:end]
        if self.calibration is not None:
            self.calibration = self.calibration[start:end]

    def copy(self):
        from copy import deepcopy
        return deepcopy(self)


class DarmASD(PEMFrequencySeries):

    def __init__(
            self, name, freqs, values, t0=None, channel=None,
            calibration=None):
        """
        Parameters
        ----------
        name : str
        freqs : array
        values : array
        t0 : float, int, optional
        unit : str, optional
        calibration : array-like, optional
        """
        super(DarmASD, self).__init__(
            name, freqs, values, t0=t0, channel=channel,
            calibration=calibration)

    @classmethod
    def get(
            cls, name, start, end, fftlength=None, overlap=None,
            calibration=None, **kwargs):
        ts = TimeSeries.get(name, start, end, **kwargs)
        asd_ = ts.asd(fftlength=fftlength, overlap=overlap)
        name = asd_.name
        freqs = asd_.frequencies.value
        value = asd_.value
        t0 = asd_.epoch.value
        channel=asd_.channel
        new = cls(
            name, freqs, value, t0=t0, channel=channel,
            calibration=calibration)
        return new

    def copy(self):
        from copy import deepcopy
        return deepcopy(self)